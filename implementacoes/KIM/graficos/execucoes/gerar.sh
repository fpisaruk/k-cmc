sh comparacao_arvores_sep_total.gnuplot 0.1 900
sh comparacao_arvores_sep_total.gnuplot 0.5 900
sh comparacao_arvores_sep_total.gnuplot 1.0 900

sh comparacao_arvores_sep_total_percentual.gnuplot 0.1 900
sh comparacao_arvores_sep_total_percentual.gnuplot 0.5 900
sh comparacao_arvores_sep_total_percentual.gnuplot 1.0 900

sh comparativo_percentual_sintetizado_by_densidade.gnuplot 900

sh comparativo_percentual_sintetizado_by_k.gnuplot 900

sh comparativo_percentual_sintetizado_by_n.gnuplot 0.1
sh comparativo_percentual_sintetizado_by_n.gnuplot 0.5
sh comparativo_percentual_sintetizado_by_n.gnuplot 1.0

sh proporcao_cm_sintetizado_by_n.gnuplot 0.1
sh proporcao_cm_sintetizado_by_n.gnuplot 0.5
sh proporcao_cm_sintetizado_by_n.gnuplot 1.0

sh proporcao_sep_sintetizado_by_n.gnuplot 0.1
sh proporcao_sep_sintetizado_by_n.gnuplot 0.5
sh proporcao_sep_sintetizado_by_n.gnuplot 1.0

sh tempo_x_n_fit.gnuplot 0.1
sh tempo_x_n_fit.gnuplot 0.5
sh tempo_x_n_fit.gnuplot 1.0

sh tempo_x_n_fit_no_residuals.gnuplot 0.1
sh tempo_x_n_fit_no_residuals.gnuplot 0.5
sh tempo_x_n_fit_no_residuals.gnuplot 1.0

sh tempo_x_k_fit_no_residuals.gnuplot 900 
sh tempo_x_k_fit.gnuplot 900
sh tempo_x_densidade_fit.gnuplot 900
