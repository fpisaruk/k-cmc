#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key bottom left box 
set key title \"densidade\" 
set mytics 2 
set mxtics 2 
set ylabel \"Propor��o\"
set xlabel \"Quantidade de caminhos gerados(k)\"
set datafile separator \";\"
set title \"Propor��o do tempo total utilizada pelas duas rotinas principais por k. n=$1\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"< /bin/sh filtra_by_d_and_n.sh 0.1 $1\" using 4:(\$5/\$7+\$6/\$7) title \"d=0.1\" w lp ls 1,\
\"< /bin/sh filtra_by_d_and_n.sh 0.5 $1\" using 4:(\$5/\$7+\$6/\$7) title \"d=0.5\" w lp ls 3,\
\"< /bin/sh filtra_by_d_and_n.sh 1.0 $1\" using 4:(\$5/\$7+\$6/\$7) title \"d=1.0\" w lp ls 5" | gnuplot > ps/comparativo_percentual_sintetizado_by_k.ps 

#\"allDensities_by_densidade.dat\" every :::1::1 using 3:(\$4/\$6+\$5/\$6) title \"d=0.2\" w lp ls 2,\
#\"allDensities_by_densidade.dat\" every :::2::2 using 3:(\$4/\$6+\$5/\$6) title \"d=0.3\" w lp ls 3,\
#\"allDensities_by_densidade.dat\" every :::3::3 using 3:(\$4/\$6+\$5/\$6) title \"d=0.4\" w lp ls 4,\
#\"allDensities_by_densidade.dat\" every :::4::4 using 3:(\$4/\$6+\$5/\$6) title \"d=0.5\" w lp ls 5,\
#\"allDensities_by_densidade.dat\" every :::5::5 using 3:(\$4/\$6+\$5/\$6) title \"d=0.6\" w lp ls 6,\
#\"allDensities_by_densidade.dat\" every :::6::6 using 3:(\$4/\$6+\$5/\$6) title \"d=0.7\" w lp ls 7,\
#\"allDensities_by_densidade.dat\" every :::3::7 using 3:(\$4/\$6+\$5/\$6) title \"d=0.8\" w lp ls 8,\
#\"allDensities_by_densidade.dat\" every :::8::8 using 3:(\$4/\$6+\$5/\$6) title \"d=0.9\" w lp ls 9,\
#\"allDensities_by_densidade.dat\" every :::9::9 using 3:(\$4/\$6+\$5/\$6) title \"d=1.0\" w lp ls 10
