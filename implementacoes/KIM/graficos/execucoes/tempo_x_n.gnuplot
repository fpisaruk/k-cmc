#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key left box 
set mytics 2 
set mxtics 2 
set xrange [100:800]
set key title \"numero de caminhos(k)\"
set ylabel \"Tempo(s)\"
set xlabel \"n�mero de v�rtices(n)\"
set title \"Tempo de execu��o em fun��o do n�mero de v�rtices usando densidade $1\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"< /bin/sh filtra_by_d_and_k.sh $1 100\" using 2:(\$7/1000) title \"100\" w p ls 1 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 200\" using 2:(\$7/1000) title \"200\" w p ls 2 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 300\" using 2:(\$7/1000) title \"300\" w p ls 3 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 400\" using 2:(\$7/1000) title \"400\" w p ls 4 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 500\" using 2:(\$7/1000) title \"500\" w p ls 5 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 600\" using 2:(\$7/1000) title \"600\" w p ls 6 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 700\" using 2:(\$7/1000) title \"700\" w p ls 7 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 800\" using 2:(\$7/1000) title \"800\" w p ls 8 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 900\" using 2:(\$7/1000) title \"900\" w p ls 9 ,\
\"< /bin/sh filtra_by_d_and_k.sh $1 1000\" using 2:(\$7/1000) title \"1000\" w p ls 10
" | gnuplot > "ps/tempo_x_n_d_$1.ps"
