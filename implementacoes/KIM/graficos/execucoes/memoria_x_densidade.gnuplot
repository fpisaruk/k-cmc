#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key left box 
set key title \"n�mero de caminhos(k)\" 
set mytics 2 
set mxtics 2 
#set xrange [0.1:1.0]
set ylabel \"Mem�ria(kbytes)\"
set xlabel \"Densidade\"
set title \"Consumo de mem�ria em fun��o da densidade\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"memoria_by_k.dat\" every :::0::0 using 5:(\$4/1024) title \"k=100\" w l ls 1,\
\"memoria_by_k.dat\" every :::1::1 using 5:(\$4/1024) title \"k=200\" w l  ls 2,\
\"memoria_by_k.dat\" every :::2::2 using 5:(\$4/1024) title \"k=300\" w l  ls 3,\
\"memoria_by_k.dat\" every :::3::3 using 5:(\$4/1024) title \"k=400\" w l  ls 4,\
\"memoria_by_k.dat\" every :::4::4 using 5:(\$4/1024) title \"k=500\" w l  ls 5,\
\"memoria_by_k.dat\" every :::5::5 using 5:(\$4/1024) title \"k=600\" w l  ls 6,\
\"memoria_by_k.dat\" every :::6::6 using 5:(\$4/1024) title \"k=700\" w l  ls 7,\
\"memoria_by_k.dat\" every :::7::7 using 5:(\$4/1024) title \"k=800\" w l  ls 8,\
\"memoria_by_k.dat\" every :::8::8 using 5:(\$4/1024) title \"k=900\" w l  ls 9,\
\"memoria_by_k.dat\" every :::9::9 using 5:(\$4/1024) title \"k=1000\" w l  ls 10" | gnuplot > ps/memoria_by_densidade.ps 
