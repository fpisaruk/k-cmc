#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key left box 
set mytics 2 
set mxtics 2 
set xrange [100:1000]
set key title \"densidade\"
set ylabel \"Tempo(s)\"
set xlabel \"Quantidade de caminhos gerados(k)\"
set title \"Tempo de execução em função do número de caminhos. n=500\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

f01(x)= a01*x
f05(x)= a05*x
f10(x)= a10*x

fit f01(x) \"< /bin/sh filtra_by_d_and_n.sh 0.1 500\"   using 4:(\$7/1000) via a01
fit f05(x) \"< /bin/sh filtra_by_d_and_n.sh 0.5 500\"   using 4:(\$7/1000) via a05
fit f10(x) \"< /bin/sh filtra_by_d_and_n.sh 1.0 500\"   using 4:(\$7/1000) via a10
 
plot 	f01(x) notitle w l ls 1,\
	f05(x) notitle w l ls 3,\
	f10(x) notitle w l ls 5,\
\"< /bin/sh filtra_by_d_and_n.sh 0.1 500\" using 4:(\$7/1000) title \"0.1\" w p ls 1 ,\
\"< /bin/sh filtra_by_d_and_n.sh 0.5 500\" using 4:(\$7/1000) title \"0.5\" w p ls 3,\
\"< /bin/sh filtra_by_d_and_n.sh 1.0 500\" using 4:(\$7/1000) title \"1.0\" w p ls 5" | gnuplot > ps/tempo_x_k.ps
