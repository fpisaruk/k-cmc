#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key left box 
set mytics 2 
set mxtics 2 
set xrange [100:1000]
set key title \"densidade\"
set ylabel \"Tempo(s)\"
set xlabel \"Quantidade de caminhos gerados(k)\"
set title \"Tempo de execu��o em fun��o do n�mero de caminhos\"
set datafile separator \";\"

f100(x)=a100*x
f200(x)=a200*x
f300(x)=a300*x
f400(x)=a400*x
f500(x)=a500*x
f600(x)=a600*x
f700(x)=a700*x
f800(x)=a800*x
f900(x)=a900*x
f1000(x)=a1000*x

fit f100(x) \"allDensities_by_densidade.dat\" every :::0::0 using 3:(\$6/1000) via a100
fit f200(x) \"allDensities_by_densidade.dat\" every :::1::1 using 3:(\$6/1000) via a200
fit f300(x) \"allDensities_by_densidade.dat\" every :::2::2 using 3:(\$6/1000) via a300
fit f400(x) \"allDensities_by_densidade.dat\" every :::3::3 using 3:(\$6/1000) via a400
fit f500(x) \"allDensities_by_densidade.dat\" every :::4::4 using 3:(\$6/1000) via a500
fit f600(x) \"allDensities_by_densidade.dat\" every :::5::5 using 3:(\$6/1000) via a600
fit f700(x) \"allDensities_by_densidade.dat\" every :::6::6 using 3:(\$6/1000) via a700
fit f800(x) \"allDensities_by_densidade.dat\" every :::7::7 using 3:(\$6/1000) via a800
fit f900(x) \"allDensities_by_densidade.dat\" every :::8::8 using 3:(\$6/1000) via a900
fit f1000(x) \"allDensities_by_densidade.dat\" every :::9::9 using 3:(\$6/1000) via a1000

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot 	f100(x) title \"0.1\" w l ls 1,\
	f200(x) title \"0.2\" w l ls 2,\
	f300(x) title \"0.3\" w l ls 3,\
	f400(x) title \"0.4\" w l ls 4,\
	f500(x) title \"0.5\" w l ls 5,\
	f600(x) title \"0.6\" w l ls 6,\
	f700(x) title \"0.7\" w l ls 7,\
	f800(x) title \"0.8\" w l ls 8,\
	f900(x) title \"0.9\" w l ls 9,\
	f1000(x) title \"1.0\" w l ls 10" | gnuplot > ps/allDensities_by_k_fit.ps 
