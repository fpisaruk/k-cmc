#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 3.0
set origin 0,0
set key left box 
set key title \"n�mero de caminhos\" 
set mytics 2 
set mxtics 2 
#set xrange [0.1:1]
set ylabel \"Tempo(s)\"
set title \"Tempo de execu��o em fun��o da densidade. n=$1.\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

f100(x)= a100*x
f200(x)= a200*x
f300(x)= a300*x
f400(x)= a400*x
f500(x)= a500*x
f600(x)= a600*x
f700(x)= a700*x
f800(x)= a800*x
f900(x)= a900*x
f1000(x)= a1000*x

fit f100(x) \"< /bin/sh filtra_by_n_and_k.sh $1 100\"   using 1:(\$7/1000) via a100
fit f200(x) \"< /bin/sh filtra_by_n_and_k.sh $1 200\"   using 1:(\$7/1000) via a200
fit f300(x) \"< /bin/sh filtra_by_n_and_k.sh $1 300\"   using 1:(\$7/1000) via a300
fit f400(x) \"< /bin/sh filtra_by_n_and_k.sh $1 400\"   using 1:(\$7/1000) via a400
fit f500(x) \"< /bin/sh filtra_by_n_and_k.sh $1 500\"   using 1:(\$7/1000) via a500
fit f600(x) \"< /bin/sh filtra_by_n_and_k.sh $1 600\"   using 1:(\$7/1000) via a600
fit f700(x) \"< /bin/sh filtra_by_n_and_k.sh $1 700\"   using 1:(\$7/1000) via a700
fit f800(x) \"< /bin/sh filtra_by_n_and_k.sh $1 800\"   using 1:(\$7/1000) via a800
fit f900(x) \"< /bin/sh filtra_by_n_and_k.sh $1 900\"   using 1:(\$7/1000) via a900
fit f1000(x) \"< /bin/sh filtra_by_n_and_k.sh $1 1000\"   using 1:(\$7/1000) via a1000

set multiplot layout 2,1 rowsfirst
set size 1.5,2.3
set origin 0,0.7


plot f100(x) notitle w l ls 1,\
	f200(x) notitle w l ls 2,\
f300(x) notitle w l ls 3,\
	f400(x) notitle w l ls 4,\
f500(x) notitle w l ls 5,\
	f600(x) notitle w l ls 6,\
f700(x) notitle w l ls 7,\
	f800(x) notitle w l ls 8,\
f900(x) notitle w l ls 9,\
	f1000(x) notitle w l ls 10,\
\"< /bin/sh filtra_by_n_and_k.sh $1 100\" using 1:(\$7/1000) title \"k=100\" w p ls 1,\
\"< /bin/sh filtra_by_n_and_k.sh $1 200\" using 1:(\$7/1000) title \"k=200\" w p ls 2,\
\"< /bin/sh filtra_by_n_and_k.sh $1 300\" using 1:(\$7/1000) title \"k=300\" w p ls 3,\
\"< /bin/sh filtra_by_n_and_k.sh $1 400\" using 1:(\$7/1000) title \"k=400\" w p ls 4,\
\"< /bin/sh filtra_by_n_and_k.sh $1 500\" using 1:(\$7/1000) title \"k=500\" w p ls 5,\
\"< /bin/sh filtra_by_n_and_k.sh $1 600\" using 1:(\$7/1000) title \"k=600\" w p ls 6,\
\"< /bin/sh filtra_by_n_and_k.sh $1 700\" using 1:(\$7/1000) title \"k=700\" w p ls 7,\
\"< /bin/sh filtra_by_n_and_k.sh $1 800\" using 1:(\$7/1000) title \"k=800\" w p ls 8,\
\"< /bin/sh filtra_by_n_and_k.sh $1 900\" using 1:(\$7/1000) title \"k=900\" w p ls 9,\
\"< /bin/sh filtra_by_n_and_k.sh $1 1000\" using 1:(\$7/1000) title \"k=1000\" w p ls 10

set size 1.5,0.7
set origin 0,0
unset key 
set ylabel \"Res�duos(s)\"
set xlabel \"Densidade do grafo\"

unset title

plot    \"< /bin/sh filtra_by_n_and_k.sh $1 100\" using 1:(abs(f100(\$1) - (\$7/1000)))  notitle  w p ls 1 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 200\" using 1:(abs(f200(\$1) - (\$7/1000)))   notitle w p ls 2 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 300\" using 1:(abs(f300(\$1) - (\$7/1000)))   notitle w p ls 3 ,\
	\"< /bin/sh filtra_by_n_and_k.sh $1 400\" using 1:(abs(f400(\$1) - (\$7/1000)))  notitle  w p ls 4 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 500\" using 1:(abs(f500(\$1) - (\$7/1000)))   notitle w p ls 5 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 600\" using 1:(abs(f600(\$1) - (\$7/1000)))   notitle w p ls 6 ,\
	\"< /bin/sh filtra_by_n_and_k.sh $1 700\" using 1:(abs(f700(\$1) - (\$7/1000)))  notitle  w p ls 7 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 800\" using 1:(abs(f800(\$1) - (\$7/1000)))   notitle w p ls 8 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 900\" using 1:(abs(f900(\$1) - (\$7/1000)))   notitle w p ls 9 ,\
        \"< /bin/sh filtra_by_n_and_k.sh $1 1000\" using 1:(abs(f1000(\$1) - (\$7/1000)))   notitle w p ls 10" | gnuplot > ps/tempo_x_densidade_fit.ps 

