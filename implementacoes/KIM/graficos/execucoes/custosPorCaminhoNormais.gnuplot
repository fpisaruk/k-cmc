#!/bin/bash
a1=$(awk -F";" 'BEGIN{sd=0;media=0;n=0;ss=0;s=0}/;0\.1/{s+=$3;ss+=$3*$3;media+=$3;n++;}END{media=media/n;sd=sqrt((ss-2*media*s+n*media*media)/n);print media ";" sd }' custosPorCaminho.dat);
a2=$(awk -F";" 'BEGIN{sd=0;media=0;n=0;ss=0;s=0}/;0\.5/{s+=$3;ss+=$3*$3;media+=$3;n++;}END{media=media/n;sd=sqrt((ss-2*media*s+n*media*media)/n);print media ";" sd }' custosPorCaminho.dat);
a3=$(awk -F";" 'BEGIN{sd=0;media=0;n=0;ss=0;s=0}/;1\.0/{s+=$3;ss+=$3*$3;media+=$3;n++;}END{media=media/n;sd=sqrt((ss-2*media*s+n*media*media)/n);print media ";" sd }' custosPorCaminho.dat);
u1=$(echo "$a1" | cut -d ";" -f1)
d1=$(echo "$a1"  | cut -d ";" -f2)
u2=$(echo "$a2" | cut -d ";" -f1)
d2=$(echo "$a2"  | cut -d ";" -f2)
u3=$(echo "$a3" | cut -d ";" -f1)
d3=$(echo "$a3"  | cut -d ";" -f2)
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key right box  
set key title \"densidade\" 
set mytics 2 
set mxtics 2 
set xrange [0:40]
set yrange [0:0.3]
set xlabel \"Tempo(ms)\"
set ylabel \"Propor��o de caminhos\"
set title \"Propor��o de caminhos que levam um certo tempo para serem calculados\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 4
set style line 2 linetype 2 linewidth 4
set style line 3 linetype 3 linewidth 4
set style line 4 linetype 4 linewidth 4
set style line 5 linetype 5 linewidth 4
set style line 6 linetype 6 linewidth 4
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 4
set style line 10 linetype 10 linewidth 4

f1(x)=exp(-(x-$u1 )**2/(2*$d1**2))/(sqrt(2*pi)*$d1 )
f2(x)=exp(-(x-$u2 )**2/(2*$d2**2))/(sqrt(2*pi)*$d2 )
f3(x)=exp(-(x-$u3 )**2/(2*$d3**2))/(sqrt(2*pi)*$d3 ) 

plot f1(x) title \"0.1\" w l ls 1,f2(x) title \"0.5\" w l ls 2,f3(x) title \"1.0\" w l ls 3" | gnuplot > ps/custosPorCaminhoNormais.ps 


