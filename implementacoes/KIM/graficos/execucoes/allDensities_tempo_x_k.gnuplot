#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key left box 
set mytics 2 
set mxtics 2 
set xrange [100:1000]
set key title \"densidade\"
set ylabel \"Tempo(s)\"
set xlabel \"Quantidade de caminhos gerados(k)\"
set title \"Tempo de execu��o em fun��o do n�mero de caminhos\"
set datafile separator \";\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"allDensities_by_densidade.dat\" every :::0::0 using 3:(\$6/1000) title \"0.1\" w l ls 1 ,\
\"allDensities_by_densidade.dat\" every :::1::1 using 3:(\$6/1000) title \"0.2\" w l ls 2 ,\
\"allDensities_by_densidade.dat\" every :::2::2 using 3:(\$6/1000) title \"0.3\" w l ls 3 ,\
\"allDensities_by_densidade.dat\" every :::3::3 using 3:(\$6/1000) title \"0.4\" w l ls 4 ,\
\"allDensities_by_densidade.dat\" every :::4::4 using 3:(\$6/1000) title \"0.5\" w l ls 5 ,\
\"allDensities_by_densidade.dat\" every :::5::5 using 3:(\$6/1000) title \"0.6\" w l ls 6 ,\
\"allDensities_by_densidade.dat\" every :::6::6 using 3:(\$6/1000) title \"0.7\" w l ls 7 ,\
\"allDensities_by_densidade.dat\" every :::7::7 using 3:(\$6/1000) title \"0.8\" w l ls 8 ,\
\"allDensities_by_densidade.dat\" every :::8::8 using 3:(\$6/1000) title \"0.9\" w l ls 9 ,\
\"allDensities_by_densidade.dat\" every :::9::9 using 3:(\$6/1000) title \"1.0\" w l ls 10" | gnuplot > ps/allDensities_by_k.ps 
