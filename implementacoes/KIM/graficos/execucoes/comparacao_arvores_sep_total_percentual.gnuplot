#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics 
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key box below 
set mytics 2 
set mxtics 2 

set ylabel \"Propor��o\"
set xlabel \"Quantidade de caminhos gerados(k)\"
set datafile separator \";\"
set title \"Comparativo entre os tempos totais e das duas principais subrotinas. densidade=$1,n=$2\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 4
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"< /bin/sh filtra_by_d_and_n.sh $1 $2\" using 4:(\$5/\$7) title \"Tempo das �rvores Ts e Tt\" w lp ls 1,\
\"< /bin/sh filtra_by_d_and_n.sh $1 $2\" using 4:(\$6/\$7) title \"Tempo da rotina SEP\" w lp ls 3 ,\
\"< /bin/sh filtra_by_d_and_n.sh $1 $2\" using 4:(\$5/\$7 + \$6/\$7) title \"Soma das duas\" w lp ls 5
" | gnuplot > ps/$0_$1.ps

 
