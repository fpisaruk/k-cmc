#!/bin/sh
echo "
set term postscript eps enhanced color 'Helvetica' 20
set grid mxtics ytics xtics mytics
set encoding iso_8859_1
set data style lines
set size 1.5, 1.5
set key top right box 
set key title \"n�mero de caminhos\" 
set mytics 2 
set mxtics 2 
set ylabel \"Propor��o\"
set xlabel \"N�mero de v�rtices\"
set datafile separator \";\"
set title \"Propor��o de tempo utilizada pelas execu��es da rotina SEP. Densidade=$1\"

set style line 1 linetype 1 linewidth 1
set style line 2 linetype 2 linewidth 1
set style line 3 linetype 3 linewidth 2
set style line 4 linetype 4 linewidth 2
set style line 5 linetype 5 linewidth 3
set style line 6 linetype 6 linewidth 3
set style line 7 linetype 7 linewidth 4
set style line 8 linetype 8 linewidth 5
set style line 9 linetype 9 linewidth 5
set style line 10 linetype 10 linewidth 5

plot \"< /bin/sh filtra_by_d_and_k.sh $1 100\" using 2:(\$6/\$7) title \"k=100\" w lp ls 1,\
\"< /bin/sh filtra_by_d_and_k.sh $1 200\" using 2:(\$6/\$7) title \"k=200\" w lp ls 2,\
\"< /bin/sh filtra_by_d_and_k.sh $1 300\" using 2:(\$6/\$7) title \"k=300\" w lp ls 3,\
\"< /bin/sh filtra_by_d_and_k.sh $1 400\" using 2:(\$6/\$7) title \"k=400\" w lp ls 4,\
\"< /bin/sh filtra_by_d_and_k.sh $1 500\" using 2:(\$6/\$7) title \"k=500\" w lp ls 5,\
\"< /bin/sh filtra_by_d_and_k.sh $1 600\" using 2:(\$6/\$7) title \"k=600\" w lp ls 6,\
\"< /bin/sh filtra_by_d_and_k.sh $1 700\" using 2:(\$6/\$7) title \"k=700\" w lp ls 7,\
\"< /bin/sh filtra_by_d_and_k.sh $1 800\" using 2:(\$6/\$7) title \"k=800\" w lp ls 8,\
\"< /bin/sh filtra_by_d_and_k.sh $1 900\" using 2:(\$6/\$7) title \"k=900\" w lp ls 9,\
\"< /bin/sh filtra_by_d_and_k.sh $1 1000\" using 2:(\$6/\$7) title \"k=1000\" w lp ls 10"| gnuplot > ps/proporcao_sep_sintetizado_by_n_d_$1.ps 

#plot \"allDensities_by_k.dat\" every :::0::0 using 7:(\$4/\$6+\$5/\$6) title \"k=100\" w lp ls 1,\
#\"allDensities_by_k.dat\" every :::2::1 using 7:(\$4/\$6+\$5/\$6) title \"k=200\" w lp ls 2,\
#\"allDensities_by_k.dat\" every :::2::2 using 7:(\$4/\$6+\$5/\$6) title \"k=300\" w lp ls 3,\
#\"allDensities_by_k.dat\" every :::3::3 using 7:(\$4/\$6+\$5/\$6) title \"k=400\" w lp ls 4,\
#\"allDensities_by_k.dat\" every :::4::4 using 7:(\$4/\$6+\$5/\$6) title \"k=500\" w lp ls 5,\
#\"allDensities_by_k.dat\" every :::5::5 using 7:(\$4/\$6+\$5/\$6) title \"k=600\" w lp ls 6,\
#\"allDensities_by_k.dat\" every :::6::6 using 7:(\$4/\$6+\$5/\$6) title \"k=700\" w lp ls 7,\
#\"allDensities_by_k.dat\" every :::7::7 using 7:(\$4/\$6+\$5/\$6) title \"k=800\" w lp ls 8,\
#\"allDensities_by_k.dat\" every :::8::8 using 7:(\$4/\$6+\$5/\$6) title \"k=900\" w lp ls 9,\
#\"allDensities_by_k.dat\" every :::9::9 using 7:(\$4/\$6+\$5/\$6) title \"k=1000\" w lp ls 10"
