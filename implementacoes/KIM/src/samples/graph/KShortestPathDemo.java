package samples.graph;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;

import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.algorithms.shortestpaths.KIM;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape.Wedge;
import edu.uci.ics.jung.visualization.renderers.Renderer;

/**
 * 
 * @author Fábio Pisaruk
 * 
 */
public class KShortestPathDemo extends JPanel {
	private static final long serialVersionUID = 6132971203691610001L;
	private KIMVertex start;
	private KIMVertex termination;
	private UndirectedGraph<KIMVertex, KIMEdge> mGraph;
	private List<KIMEdge> path;
	private Vector<KIMVertex> verticesOnPath;
	private Transformer<KIMEdge, Number> nev;
	private VisualizationViewer<KIMVertex, KIMEdge> vv;
	private static BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();

	boolean isBlessed(KIMEdge e) {
		// Vertex v1 = (Vertex) e.getEndpoints().getFirst();
		// Vertex v2 = (Vertex) e.getEndpoints().getSecond();
		return (path != null) && path.contains(e);
	}

	public class MyEdgePaintFunction implements Transformer<KIMEdge, Paint> {

		@Override
		public Paint transform(KIMEdge e) {
			if (path == null || path.size() == 0)
				return Color.BLACK;
			if (isBlessed(e)) {
				return new Color(0.0f, 0.0f, 1.0f, 0.5f);// Color.BLUE;
			} else {
				return Color.LIGHT_GRAY;
			}
		}
	}

	public class MyEdgeStrokeFunction implements Transformer<KIMEdge, Stroke> {
		protected final Stroke THIN = new BasicStroke(1);
		protected final Stroke THICK = new BasicStroke(1);

		@Override
		public Stroke transform(KIMEdge e) {
			if (path == null || path.size() == 0)
				return THIN;
			if (isBlessed(e)) {
				return THICK;
			} else
				return THIN;
		}
	}

	/**
	 * @author danyelf
	 */
	public class MyVertexPaintFunction implements Transformer<KIMVertex, Paint> {

		@Override
		public Paint transform(KIMVertex arg0) {
			return Color.black;
		}
	}

	public class MyEdgeLabelTransformer implements Transformer<KIMEdge, String> {

		@Override
		public String transform(KIMEdge e) {
			return nev.transform(e).toString();
		}

	}

	public class MyVertexFillFunction implements Transformer<KIMVertex, Paint> {

		@Override
		public Paint transform(KIMVertex v) {
			if (v == start) {
				return Color.BLUE;
			}
			if (v == termination) {
				return Color.BLUE;
			}
			if (path == null) {
				return Color.LIGHT_GRAY;
			} else {
				if (verticesOnPath.contains(v)) {
					return Color.RED;
				} else {
					return Color.LIGHT_GRAY;
				}
			}

		}
	}

	public class MyEdgeLabelClosenessTransformer implements
			Transformer<Context<Graph<KIMVertex, KIMEdge>, KIMEdge>, Number> {
		@Override
		public Number transform(Context<Graph<KIMVertex, KIMEdge>, KIMEdge> arg0) {
			return 0.65;
		}

	}

	/**
	 * @param g
	 */
	public KShortestPathDemo(UndirectedGraph<KIMVertex, KIMEdge> g,
			Transformer<KIMEdge, Number> nev) {
		this.mGraph = g;
		this.nev = nev;
		setBackground(Color.WHITE);
		// show graph
		final Layout<KIMVertex, KIMEdge> layout = new KKLayout<KIMVertex, KIMEdge>(
				g);
		vv = new VisualizationViewer<KIMVertex, KIMEdge>(layout);
		vv.setBackground(Color.WHITE);
		// mGD = new GraphDraw(layout);
		// mGD.setBackground(Color.WHITE);
		// mGD.hideStatus();

		// final VisualizationViewer vv = mGD.getVisualizationViewer();
		vv.getRenderContext().setVertexDrawPaintTransformer(
				new MyVertexPaintFunction());
		vv.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());
		vv.getRenderContext().setEdgeLabelTransformer(
				new MyEdgeLabelTransformer());

		vv.getRenderContext().setEdgeFontTransformer(
				new Transformer<KIMEdge, Font>() {
					@Override
					public Font transform(KIMEdge arg0) {
						return new Font(Font.SANS_SERIF, Font.BOLD, 12);
					}
				});
		vv.getRenderContext().setEdgeLabelClosenessTransformer(
				new MyEdgeLabelClosenessTransformer());

		vv.getRenderContext().setEdgeShapeTransformer(new Wedge(2));

		vv.getRenderContext().setEdgeStrokeTransformer(
				new MyEdgeStrokeFunction());
		vv.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex v) {
						return v.getNome();
					}
				});

		// vv.setPickSupport(new ShapePickSupport());
		vv.setGraphMouse(new DefaultModalGraphMouse<KIMVertex, KIMEdge>());
		vv.addPostRenderPaintable(new VisualizationViewer.Paintable() {

			public boolean useTransform() {
				return true;
			}

			public void paint(Graphics g) {
				if (path == null)
					return;
				// for all edges, paint edges that are in shortest path
				for (KIMEdge e : layout.getGraph().getEdges()) {
					if (isBlessed(e)) {
						KIMVertex v1 = mGraph.getEndpoints(e).getFirst();
						KIMVertex v2 = mGraph.getEndpoints(e).getSecond();
						Point2D p1 = layout.transform(v1);
						Point2D p2 = layout.transform(v2);
						p1 = vv.getRenderContext().getMultiLayerTransformer()
								.transform(Layer.LAYOUT, p1);
						p2 = vv.getRenderContext().getMultiLayerTransformer()
								.transform(Layer.LAYOUT, p2);
						Renderer<KIMVertex, KIMEdge> renderer = vv
								.getRenderer();
						renderer.renderEdge(vv.getRenderContext(), layout, e);
					}
				}
			}
		});

		setLayout(new BorderLayout());
		// add(mGD, BorderLayout.CENTER);
		add(vv, BorderLayout.CENTER);
		// set up controls
		add(setUpControls(), BorderLayout.SOUTH);
	}

	public void saveGraphAsJPEG(VisualizationViewer vv, File file) {
		// BufferedImage myImage = new BufferedImage(vv.getWidth(),
		// vv.getHeight(), BufferedImage.TYPE_INT_RGB);
		// Graphics2D g2 = myImage.createGraphics();
		// Color bg = vv.getBackground();
		// g2.setColor(bg);
		// g2.fillRect(0, 0, vv.getWidth(), vv.getHeight());
		vv.setDoubleBuffered(true);
		// vv.paintAll(g2);
		try {
			// ImageIO.write(myImage, "gif", new File(filename));
			FileOutputStream fos = new FileOutputStream(file);
			EpsGraphics epsGraphics = new EpsGraphics("TESTE", fos, 0, 0, vv
					.getWidth(), vv.getHeight(), ColorMode.COLOR_RGB);
			vv.paintAll(epsGraphics);
			fos.flush();
			epsGraphics.close();
		} catch (Throwable e) {
			System.out.println(e);
		}
	}

	/**
	 *  
	 */
	private JPanel setUpControls() {
		final JPanel jp = new JPanel();
		jp.setBackground(Color.WHITE);
		jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
		JButton salvar = new JButton("Salvar como eps");
		salvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				if (fc.showSaveDialog(jp) == JFileChooser.APPROVE_OPTION) {
					saveGraphAsJPEG(vv, fc.getSelectedFile());
				}
			}
		});
		jp.add(salvar);
		jp.setBorder(BorderFactory.createLineBorder(Color.black, 3));
		jp
				.add(new JLabel(
						"Selecione um par de vértices para calcular o menor caminho usando Dijkstra"));
		JPanel jp2 = new JPanel();
		jp2.add(new JLabel("origem", SwingConstants.LEFT));
		jp2.add(getSelectionBox(true));
		jp2.setBackground(Color.white);
		JPanel jp3 = new JPanel();
		jp3.add(new JLabel("destino", SwingConstants.LEFT));
		jp3.add(getSelectionBox(false));
		jp3.setBackground(Color.white);
		jp.add(jp2);
		jp.add(jp3);
		return jp;
	}

	/**
	 * @param grafo
	 * @param from
	 * @return
	 */
	private Component getSelectionBox(final boolean from) {
		Set<String> s = new TreeSet<String>();
		for (KIMVertex v : mGraph.getVertices())
			s.add(v.getNome());
		final JComboBox choices = new JComboBox(s.toArray());
		choices.setSelectedIndex(-1);
		choices.setBackground(Color.WHITE);
		choices.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				KIMVertex v = bMap.getKey((String) choices.getSelectedItem());
				if (from) {
					start = v;
					// System.out.println("Assigned mFrom!");
				} else {
					termination = v;
					// System.out.println("Assigned mTo!");
				}
				drawShortest();
				repaint();
			}
		});
		return choices;
	}

	protected void drawShortest() {
		if (start == null || termination == null) {
			return;
		}
		ShortestPathKIM<KIMVertex, KIMEdge> sp = new ShortestPathKIM.Factory(
				nev, mGraph, ShortestPathKIM.BFSShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TS);
		path = sp.getPath(start, termination);
		Iterator<KIMEdge> i = path.iterator();
		KIMVertex cur = start;
		verticesOnPath = new Vector<KIMVertex>(path.size() + 1);
		while (i.hasNext()) {
			KIMEdge edge = i.next();
			KIMVertex follow = mGraph.getOpposite(cur, edge);
			verticesOnPath.add(follow);
			cur = follow;
		}
	}

	public static void main(String[] s) throws IOException {
		if (s.length != 4) {
			printUsage();
			return;
		}
		String fileName = s[0];
		String origem = s[1];
		String destino = s[2];
		int k = Integer.parseInt(s[3]);
		if (k < 1)
			throw new IllegalArgumentException(
					"Escolhe um valor de k maior que zero!");
		//String alg = s[5];
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		g = pnr.load(fileName, g);
		BidiMap<String, KIMVertex> map = new DualHashBidiMap<String, KIMVertex>();
		for (KIMVertex v : g.getVertices()) {
			v.setNome(pnr.getVertexLabeller().transform(v));
			map.put(v.getNome(), v);
		}
		KIMVertex source = map.get(origem);
		KIMVertex target = map.get(destino);
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		kim.getPaths(source, target, k);
		/*
		 * Transformer<KIMEdge, Number> nev = KIMEdge.getConstantTransformer();
		 * UndirectedGraph<KIMVertex, KIMEdge> g = getGraph(nev); JFrame jf =
		 * new JFrame(); jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 * jf.getContentPane().add(new KShortestPathDemo(g, nev)); jf.pack();
		 * jf.setVisible(true);
		 */

	}

	private static void printUsage() {
		System.out.println("Uso: KShortestPathDemo file origem destino k\n"
				+ "\t origem\n" + "\t destino\n"
				+ "\t n Número de caminhos a serem gerados\n");
	}

	static UndirectedGraph<KIMVertex, KIMEdge> getGraph(
			Transformer<KIMEdge, Number> nev) {
		PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pajekNetReader = new PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		try {
			g = (UndirectedGraph<KIMVertex, KIMEdge>) pajekNetReader.load(
					"data/t.dat", g);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (KIMVertex v : g.getVertices()) {
			v.setNome(pajekNetReader.getVertexLabeller().transform(v));
			bMap.put(v, v.getNome());
		}
		return g;
	}
}
