/**
 * 
 */
package samples.graph;

import hep.aida.ref.Test;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Tree;

/**
 * @author Fábio Pisaruk
 * 
 */
public class Teste<T extends KIMVertex> {
	T valor;

	public Teste(T fabio) {
		valor = fabio;
	}

	public static void main(String[] args) {
		Teste<KIMVertex> t = new Teste<KIMVertex>(new KIMVertex("a", false));

		Tree<String, Number> g = new DelegateTree<String, Number>();
		g.addVertex("a");
		g.addEdge(1, "a", "b");
		g.addEdge(2, "a", "c");
		g.addEdge(3, "a", "d");
		System.out.println(g);
	}
}
