package edu.uci.ics.jung.algorithms.shortestpaths;

import java.util.List;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.util.Pair;

public interface Path<V extends KIMVertex, E extends KIMEdge> {
	public abstract int getIteracao();

	public abstract void setIteracao(int iteracao);

	public abstract Double getCost();

	public abstract KIMVertex getDevVertex();

	public abstract int getDevNodeIndex();

	public abstract KIMEdge getEdge(int pos);

	public abstract List<KIMEdge> getEdges();

	public abstract int getOrdem();

	public abstract String getOrigem();

	public abstract Path<KIMVertex, KIMEdge> getParent();

	public abstract Path<KIMVertex, KIMEdge> getPrefix(KIMVertex last);

	public abstract Path<KIMVertex, KIMEdge> getReverse();

	public abstract KIMVertex getStart();

	public abstract Path<KIMVertex, KIMEdge> getSubPath(KIMVertex v);

	public abstract Path<KIMVertex, KIMEdge> getSubPath(KIMVertex v, int from,
			int to);

	public abstract KIMVertex getTarget();

	public abstract KIMVertex getVertex(int pos);

	public abstract List<KIMVertex> getVertices();

	public abstract void setOrdem(int ordem);

	public abstract void setOrigem(String origem);

	public abstract void setParent(Path<KIMVertex, KIMEdge> parent);

	public class Factory {
		private UndirectedGraph<KIMVertex, KIMEdge> g;

		public UndirectedGraph<KIMVertex, KIMEdge> getGraph() {
			return g;
		}

		public Transformer<KIMEdge, Number> getTransformer() {
			return nev;
		}

		private Transformer<KIMEdge, Number> nev;

		public Factory(UndirectedGraph<KIMVertex, KIMEdge> g,
				Transformer<KIMEdge, Number> nev) {
			this.g = g;
			this.nev = nev;
		}

		public final Path<KIMVertex, KIMEdge> newInstance(
				Path<KIMVertex, KIMEdge> p1, Path<KIMVertex, KIMEdge> p2) {
			return new BasicPath<KIMVertex, KIMEdge>(p1, p2, g, nev);
		}

		public final Path<KIMVertex, KIMEdge> newInstance(List<KIMEdge> edges,
				KIMVertex vertex) {
			return new BasicPath<KIMVertex, KIMEdge>(edges, vertex, g, nev);
		}

		public final Path<KIMVertex, KIMEdge> newInstance(
				ShortestPathKIM<KIMVertex, KIMEdge> Ts,
				ShortestPathKIM<KIMVertex, KIMEdge> Tt, Pair pair, KIMVertex s,
				KIMVertex t) {
			return new BasicPath<KIMVertex, KIMEdge>(Ts, Tt, pair, s, t, g, nev);
		}
	}

}