package edu.uci.ics.jung.algorithms.shortestpaths;

import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;

/**
 * @author Fábio Pisaruk
 * 
 */

public interface ShortestSimplePaths<V extends KIMVertex> {

	public abstract Path<KIMVertex, KIMEdge>[] getPaths(V source, V target,
			int numberOfPaths);

}
