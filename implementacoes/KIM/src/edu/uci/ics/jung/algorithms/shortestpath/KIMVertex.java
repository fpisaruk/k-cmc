/**
 * 
 */
package edu.uci.ics.jung.algorithms.shortestpath;

import org.apache.commons.collections15.Factory;

public class KIMVertex {
	public static class KIMVertexFactory implements Factory<KIMVertex> {
		int id = 1;

		@Override
		public KIMVertex create() {
			return new KIMVertex(Integer.toString(id++));
		}
	}
	public static Factory<KIMVertex> getFactory() {
		return new KIMVertex.KIMVertexFactory();
	}
	private Integer epsilon;
	private boolean isOnPath = false;
	private String nome;

	private Integer zeta;

	public KIMVertex(String nome) {
		this(nome, false);
	}
	public KIMVertex(String nome, boolean isOnPath) {
		this.nome = nome;
		this.isOnPath = isOnPath;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		try {
			KIMVertex other = (KIMVertex) obj;
			return getNome().equals(other.getNome());
		} catch (ClassCastException e) {
			return false;
		}
	}
	public Integer getEpsilon() {
		return epsilon;
	}
	public String getNome() {
		return nome;
	}
	public Integer getZeta() {
		return zeta;
	}
	public boolean isOnPath() {
		return isOnPath;
	}
	public void setEpsilon(Integer epsilon) {
		this.epsilon = epsilon;
	}
	public void setIsOnPath(boolean isOnPath) {
		this.isOnPath = isOnPath;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setZeta(Integer zeta) {
		this.zeta = zeta;
	}
	public String toString() {
		return nome;
	}
}
