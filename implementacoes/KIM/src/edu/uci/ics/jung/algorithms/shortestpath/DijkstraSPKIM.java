package edu.uci.ics.jung.algorithms.shortestpath;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections15.Transformer;
import org.apache.log4j.Logger;

import edu.uci.ics.jung.graph.UndirectedGraph;

/**
 * @author Fábio Pisaruk
 * 
 */
public class DijkstraSPKIM<V extends KIMVertex, E extends KIMEdge>
		extends DijkstraShortestPath<V, E>
		implements ShortestPathKIM<V, E> {

	private HashMap<V, LinkedHashSet<V>> sons = new HashMap<V, LinkedHashSet<V>>();

	static Logger logger = Logger
			.getLogger(DijkstraSPKIM.class);
	private ShortestPathKIM.TreeType treeType;

	public ShortestPathKIM.TreeType getTreeType() {
		return treeType;
	}

	public void setTreeType(
			ShortestPathKIM.TreeType treeType) {
		this.treeType = treeType;
	}

	@Override
	protected SourceData getSourceData(V source) {
		SourceData sd = (SourcePathDataKIM) sourceMap
				.get(source);
		if (sd == null)
			sd = new SourcePathDataKIM(source);
		return sd;
	}

	public DijkstraSPKIM(UndirectedGraph<V, E> g,
			Transformer<E, Number> nev,
			ShortestPathKIM.TreeType treeType) {
		super(g, nev);
		this.treeType = treeType;
	}

	protected class SourcePathDataKIM
			extends SourcePathData {

		@Override
		public Entry<V, Number> getNextVertex() {
			Map.Entry<V, Number> p = super.getNextVertex();
			V v = p.getKey();
			E incomingEdge = incomingEdges.get(v);
			if (incomingEdge != null) {
				V pred = ((UndirectedGraph<V, E>) g)
						.getOpposite(v, incomingEdges
								.get(v));
				setLabel(v, pred);
				sons.get(pred).add(v);
			}
			sons.put(v, new LinkedHashSet<V>());
			return p;
		}

		@Override
		public void createRecord(V w, E e, double new_dist) {
			V pred = ((UndirectedGraph<V, E>) g)
					.getOpposite(w, e);
			if (w.isOnPath()) {
				if (!pred.isOnPath())
					return;
				if (getTreeType().equals(
						ShortestPathKIM.TreeType.TS)) {
					if (w.getEpsilon().intValue()
							- pred.getEpsilon().intValue() != 1)
						return;
				} else if (getTreeType().equals(
						ShortestPathKIM.TreeType.TT)) {
					if (w.getZeta().intValue()
							- pred.getZeta().intValue() != -1)
						return;
				}
			}
			super.createRecord(w, e, new_dist);
		}

		private void setLabel(V w, V pred) {
			if (!w.isOnPath()) {
				if (getTreeType().equals(
						ShortestPathKIM.TreeType.TS))
					w.setEpsilon(pred.getEpsilon());
				else if (getTreeType().equals(
						ShortestPathKIM.TreeType.TT))
					w.setZeta(pred.getZeta());
			}
		}

		public SourcePathDataKIM(V source) {
			super(source);
		}
	}

	@Override
	public LinkedHashSet<V> getSons(V source, V vertex) {
		return sons.get(vertex);
	}
}