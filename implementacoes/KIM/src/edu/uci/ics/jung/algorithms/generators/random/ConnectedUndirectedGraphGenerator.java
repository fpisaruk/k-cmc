package edu.uci.ics.jung.algorithms.generators.random;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections15.Factory;

import edu.uci.ics.jung.algorithms.generators.GraphGenerator;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

public class ConnectedUndirectedGraphGenerator<V extends KIMVertex, E extends KIMEdge>
		implements GraphGenerator<KIMVertex, KIMEdge> {

	private Factory<KIMVertex> vertexFactory;
	private Factory<KIMEdge> edgeFactory;
	private int n;
	private int m;
	private Random mRandom;

	@Override
	public Graph<KIMVertex, KIMEdge> create() {
		UndirectedGraph<KIMVertex, KIMEdge> graph = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		KIMVertex prior = vertexFactory.create();
		KIMVertex first = prior;
		graph.addVertex(prior);
		for (int i = 2; i <= n; i++) {
			KIMVertex cur = vertexFactory.create();
			graph.addVertex(cur);
			graph.addEdge(edgeFactory.create(), prior, cur);
			prior = cur;
		}
		graph.addEdge(edgeFactory.create(), prior, first);
		List<KIMVertex> vertices = new ArrayList<KIMVertex>(graph.getVertices());
		while (graph.getEdgeCount() < m) {
			KIMVertex u = vertices.get((int) (mRandom.nextDouble() * n));
			KIMVertex v = vertices.get((int) (mRandom.nextDouble() * n));
			if (!v.equals(u) && !graph.isSuccessor(v, u)) {
				graph.addEdge(edgeFactory.create(), u, v);
			}
		}
		return graph;
	}

	public ConnectedUndirectedGraphGenerator(int n, double densidade) {
		this(n, ((int) (densidade * n * (n - 1) / 2)));
	}

	public ConnectedUndirectedGraphGenerator(Factory<KIMVertex> vertexFactory,
			Factory<KIMEdge> edgeFactory, int n, int m) {
		if (m < n)
			throw new IllegalArgumentException(
					"Numero de arcos deve ser no m�nimo igual ao "
							+ "numero de vertices");
		if (m > (n * (n - 1) / 2))
			throw new IllegalArgumentException("N�o � poss�vel criar " + m
					+ " arcos de modo que n�o haja arcos "
					+ "paralelos e loops num grafo com " + n + " v�rtices");
		this.vertexFactory = vertexFactory;
		this.edgeFactory = edgeFactory;
		this.n = n;
		this.m = m;
		mRandom = new Random();
	}

	public ConnectedUndirectedGraphGenerator(int n, int m) {
		this(KIMVertex.getFactory(), KIMEdge.getFactory(), n, m);
	}

	public static void atribuiCustosNasArestas(Graph<?, KIMEdge> grafo,
			double min, double max) {
		if (min > max)
			throw new IllegalArgumentException("min deve ser inferior a max");
		if (min < 0)
			throw new IllegalArgumentException("min deve ser maior que zero");
		Iterator<KIMEdge> i = grafo.getEdges().iterator();
		Random random = new Random();
		while (i.hasNext()) {
			KIMEdge edge = i.next();
			edge.setCost(random.nextDouble() * (max - min) + min);
		}
	}
}