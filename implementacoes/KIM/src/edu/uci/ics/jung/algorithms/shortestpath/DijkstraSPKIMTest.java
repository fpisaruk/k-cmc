/**
 * 
 */
package edu.uci.ics.jung.algorithms.shortestpath;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM.TreeType;
import edu.uci.ics.jung.algorithms.shortestpaths.KIMTest;
import edu.uci.ics.jung.algorithms.transformation.UndirectGraphToTree;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.utils.draw.KIMGraphDraw;

/**
 * @author Fábio Pisaruk
 * 
 */
public class DijkstraSPKIMTest {
	static Logger logger = Logger.getLogger(KIMTest.class);


	/**
	 * Test method for
	 * {@link edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath#getPath(edu.uci.ics.jung.graph.Vertex, edu.uci.ics.jung.graph.Vertex)}
	 * .
	 * 
	 * @throws CloneNotSupportedException
	 */
	@Test
	public final void testGetPath()
			throws CloneNotSupportedException {
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		KIMVertex a = new KIMVertex("a");
		KIMVertex b = new KIMVertex("b");
		KIMVertex c = new KIMVertex("c");
		KIMVertex d = new KIMVertex("d");
		KIMVertex e = new KIMVertex("e");
		g.addVertex(a);
		g.addVertex(b);
		g.addVertex(c);
		g.addVertex(d);
		g.addVertex(e);
		g.addEdge(new KIMEdge(1.0), a, b);
		g.addEdge(new KIMEdge(1.0), a, c);
		g.addEdge(new KIMEdge(1.0), a, d);
		g.addEdge(new KIMEdge(1.0), b, c);
		g.addEdge(new KIMEdge(1.5), b, e);
		g.addEdge(new KIMEdge(3.0), c, e);
		g.addEdge(new KIMEdge(2.0), c, d);
		g.addEdge(new KIMEdge(1.0), d, e);
		System.out.println("CONTINUANDO");
		Transformer<KIMEdge, Number> wtTransformer = KIMEdge
				.getDefautTransformer();
		logger.debug(g.getVertices());
		logger.debug(g.getEdges());
		DijkstraSPKIM<KIMVertex, KIMEdge> dspKIM = new DijkstraSPKIM<KIMVertex, KIMEdge>(
				g, wtTransformer, ShortestPathKIM.TreeType.TS);
		a.setIsOnPath(true);
		d.setIsOnPath(true);
		e.setIsOnPath(true);
		a.setEpsilon(1);
		d.setEpsilon(2);
		e.setEpsilon(3);
		a.setZeta(1);
		d.setZeta(2);
		e.setZeta(3);
		List<KIMEdge> p1 = dspKIM.getPath(a, e);
		Assert.assertEquals(a.getEpsilon(), 1,0);
		Assert.assertEquals(b.getEpsilon(), 1,0);
		Assert.assertEquals(c.getEpsilon(), 1,0);
		Assert.assertEquals(d.getEpsilon(), 2,0);
		Assert.assertEquals(e.getEpsilon(), 3,0);
		Iterator<KIMEdge> i = p1.iterator();
		Set<KIMVertex> vertices = new HashSet<KIMVertex>();
		while (i.hasNext()) {
			KIMEdge edge = (KIMEdge) i.next();
			System.out.println(edge + " -> "
					+ g.getEndpoints(edge));
			vertices.add(g.getEndpoints(edge).getFirst());
			vertices.add(g.getEndpoints(edge).getSecond());
		}
		Assert.assertTrue(vertices.contains(a));
		Assert.assertTrue(vertices.contains(d));
		Assert.assertTrue(vertices.contains(e));
		Assert.assertFalse(vertices.contains(b));
		Assert.assertFalse(vertices.contains(c));
		Map<KIMVertex, KIMEdge> edgeMap = dspKIM
				.getIncomingEdgeMap(a);
		Tree<KIMVertex, KIMEdge> TS = new UndirectGraphToTree(
				a, edgeMap).transform(g);
		dspKIM = new DijkstraSPKIM<KIMVertex, KIMEdge>(g,
				wtTransformer, ShortestPathKIM.TreeType.TT);
		edgeMap = dspKIM.getIncomingEdgeMap(e);
		Assert.assertEquals(a.getZeta(), 1,0);
		Assert.assertEquals(b.getZeta(), 3,0);
		Assert.assertEquals(c.getZeta(), 3,0);
		Assert.assertEquals(d.getZeta(), 2,0);
		Assert.assertEquals(e.getZeta(), 3,0);
		Tree<KIMVertex, KIMEdge> TT = new UndirectGraphToTree(
				e, edgeMap).transform(g);
		KIMGraphDraw.desenha(g, TS, TT, wtTransformer);
	}

	@Test
	public final void testMostraErroCustoZeroNasArestas()
			throws IOException {
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge
						.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr
				.getEdgeWeightTransformer();
		g = pnr.load("data/pajNetTestComZero.dat", g);
		Iterator<KIMVertex> i = g.getVertices().iterator();
		BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			atual.setNome(pnr.getVertexLabeller()
					.transform(atual));
			bMap.put(atual, atual.getNome());
		}
		Iterator<KIMEdge> j = g.getEdges().iterator();
		while (j.hasNext()) {
			KIMEdge atual = j.next();
			logger.debug(atual + " -> "
					+ nev.transform(atual));
		}
		bMap.getKey("a").setEpsilon(1);
		bMap.getKey("b").setEpsilon(2);
		bMap.getKey("d").setEpsilon(3);
		bMap.getKey("e").setEpsilon(4);
		bMap.getKey("a").setZeta(1);
		bMap.getKey("b").setZeta(2);
		bMap.getKey("d").setZeta(3);
		bMap.getKey("e").setZeta(4);
		bMap.getKey("a").setIsOnPath(true);
		bMap.getKey("b").setIsOnPath(true);
		bMap.getKey("d").setIsOnPath(true);
		bMap.getKey("e").setIsOnPath(true);
		int k = 700;
		Collection<KIMEdge> edges = g.getIncidentEdges(bMap
				.getKey("b"));
		Iterator<KIMEdge> it = edges.iterator();
		KIMEdge bc = null;
		while (it.hasNext()) {
			KIMEdge edge = (KIMEdge) it.next();
			if (g.getEndpoints(edge).contains(
					bMap.getKey("b"))
					&& g.getEndpoints(edge).contains(
							bMap.getKey("c"))) {
				bc = edge;
				break;
			}
		}
		g.removeEdge(bc);
		DijkstraSPKIM<KIMVertex, KIMEdge> Ts = new DijkstraSPKIM<KIMVertex, KIMEdge>(
				g, nev, ShortestPathKIM.TreeType.TS);
		Ts.getIncomingEdgeMap(bMap.getKey("a"));
		g.addEdge(bc, bMap.getKey("b"), bMap.getKey("c"));

		edges = g.getIncidentEdges(bMap.getKey("d"));
		it = edges.iterator();
		KIMEdge dc = null;
		while (it.hasNext()) {
			KIMEdge edge = (KIMEdge) it.next();
			if (g.getEndpoints(edge).contains(
					bMap.getKey("d"))
					&& g.getEndpoints(edge).contains(
							bMap.getKey("c"))) {
				dc = edge;
				break;
			}
		}
		g.removeEdge(dc);

		DijkstraSPKIM<KIMVertex, KIMEdge> Tt = new DijkstraSPKIM<KIMVertex, KIMEdge>(
				g, nev, ShortestPathKIM.TreeType.TT);
		Tt.getIncomingEdgeMap(bMap.getKey("e"));
		g.addEdge(dc, bMap.getKey("d"), bMap.getKey("c"));

		KIMGraphDraw.desenha(g, Ts, bMap.getKey("a"), Tt,
				bMap.getKey("e"), nev);
	}
}
