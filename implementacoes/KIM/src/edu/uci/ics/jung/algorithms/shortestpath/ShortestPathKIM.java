package edu.uci.ics.jung.algorithms.shortestpath;

import java.lang.reflect.Constructor;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.graph.UndirectedGraph;

public interface ShortestPathKIM<V extends KIMVertex, E extends KIMEdge>
		extends ShortestPath<V, E>, Distance<V> {
	public static final Class BFSShortestPathKIMAlgorithm = BFSKIM.class;
	public static final Class DijkstraShortestPathKIMAlgorithm = DijkstraSPKIM.class;

	public List<E> getPath(V source, V target);

	public E getIncomingEdge(V source, V target);

	public LinkedHashSet<V> getSons(V source, V vertex);

	public class Factory {
		Transformer<KIMEdge, Number> nev;
		UndirectedGraph<KIMVertex, KIMEdge> g;
		Class shortestPathKIM;
		public Constructor construtor;

		public Factory(Transformer<KIMEdge, Number> nev,
				UndirectedGraph<KIMVertex, KIMEdge> g,
				Class<ShortestPathKIM<KIMVertex, KIMEdge>> shortestPathKIM) {
			this.nev = nev;
			this.g = g;
			this.shortestPathKIM = shortestPathKIM;
			try {
				construtor = shortestPathKIM.getConstructor(new Class[] {
						UndirectedGraph.class, Transformer.class,
						ShortestPathKIM.TreeType.class });
			} catch (Exception e) {
				RuntimeException err = new RuntimeException();
				err.initCause(e);
				throw err;
			}
		}

		public ShortestPathKIM<KIMVertex, KIMEdge> newInstance(
				ShortestPathKIM.TreeType treeType) {
			try {
				return (ShortestPathKIM<KIMVertex, KIMEdge>) construtor
						.newInstance(new Object[] { g, nev, treeType });
			} catch (Exception e) {
				RuntimeException err = new RuntimeException();
				err.initCause(e);
				throw err;
			}
		}
	}

	public static class TreeType {
		public String getValor() {
			return valor;
		}

		public void setValor(String valor) {
			this.valor = valor;
		}

		@Override
		public String toString() {
			return valor;
		}

		@Override
		public boolean equals(Object obj) {
			return this.getValor().equals(((TreeType) obj).getValor());
		}

		public static final TreeType TS = new TreeType("TS");
		public static final TreeType TT = new TreeType("TT");
		private String valor;

		private TreeType(String val) {
			this.valor = val;
		};

	}
}
