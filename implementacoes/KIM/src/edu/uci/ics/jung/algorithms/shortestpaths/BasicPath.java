package edu.uci.ics.jung.algorithms.shortestpaths;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.util.Pair;

public class BasicPath<V extends KIMVertex, E extends KIMEdge> implements
		Comparable<BasicPath<V, E>>, Path<V, E> {

	private double cost = 0;
	private Integer devNodeIndex;
	private List<KIMEdge> edges;
	private UndirectedGraph<KIMVertex, KIMEdge> g;
	private int iteracao = -1;
	private Transformer<KIMEdge, Number> nev;
	private int ordem = -1;
	private String origem;
	private Path<KIMVertex, KIMEdge> parent;
	private List<KIMVertex> vertices;

	protected BasicPath(List<KIMEdge> edges, KIMVertex vertex,
			UndirectedGraph<KIMVertex, KIMEdge> g, Transformer nev) {
		this(g, nev);
		this.edges = edges;
		setVertices(vertex);
	}

	/**
	 * Gera um caminho a partir da concatenacao de dois outros
	 * 
	 * @param p1
	 * @param p2
	 */
	protected BasicPath(Path<KIMVertex, KIMEdge> p1,
			Path<KIMVertex, KIMEdge> p2, UndirectedGraph<KIMVertex, KIMEdge> g,
			Transformer nev) {
		this(g, nev);
		edges = new ArrayList<KIMEdge>(p1.getEdges().size()
				+ p2.getEdges().size());
		edges.addAll(p1.getEdges());
		edges.addAll(p2.getEdges());
		setVertices((KIMVertex) p1.getStart());
	}

	protected BasicPath(ShortestPathKIM<KIMVertex, KIMEdge> Ts,
			ShortestPathKIM<KIMVertex, KIMEdge> Tt, Pair pair, KIMVertex s,
			KIMVertex t, UndirectedGraph<KIMVertex, KIMEdge> g,
			Transformer<KIMEdge, Number> nev) {
		this(g, nev);
		List<KIMEdge> ini, fim;
		ini = Ts.getPath(s, (KIMVertex) pair.getFirst());
		cost = Ts.getDistance(s, (KIMVertex) pair.getFirst()).doubleValue();
		if (pair.getFirst().equals(pair.getSecond())) {
			fim = Tt.getPath(t, (KIMVertex) pair.getFirst());
			cost += Tt.getDistance(t, (KIMVertex) pair.getFirst())
					.doubleValue();
		} else {
			ini.add((KIMEdge) pair.getSecond());
			cost += nev.transform((KIMEdge) pair.getSecond()).doubleValue();
			cost += Tt.getDistance(t, (KIMVertex) pair.getFirst())
					.doubleValue();
			fim = Tt.getPath(t, g.getOpposite((KIMVertex) pair.getFirst(),
					(KIMEdge) pair.getSecond()));
		}
		for (int i = fim.size() - 1; i >= 0; i--)
			ini.add(fim.get(i));
		edges = ini;
		setVertices(s);
	}

	private BasicPath(UndirectedGraph<KIMVertex, KIMEdge> g, Transformer nev) {
		this.g = g;
		this.nev = nev;
	}

	@Override
	public int compareTo(BasicPath<V, E> o) {
		int i = this.getCost().compareTo(o.getCost());
		if (i == 0) {
			if (o.equals(this))
				return 0;
			if (parent == null)
				return -1;
			if (o.getParent() == null)
				return 1;
			int j = ((Integer) getParent().getOrdem()).compareTo(o.getParent()
					.getOrdem());
			if (j == 0)
				return -1;
			return j;
		}
		return i;
	}

	/**
	 * Calcula o desvio deste no em relacao a p
	 * 
	 * @param p
	 *            Caminho a partir do qual este n� se desvia
	 */
	private void computeDeviation() {
		if (parent != null)
			for (int i = 0; i < vertices.size(); i++) {
				if (!parent.getVertices().get(i).equals(vertices.get(i))) {
					devNodeIndex = i - 1;
					break;
				}
			}
		else
			devNodeIndex = -1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#equals(java.lang
	 * .Object)
	 */
	public boolean equals(Object arg0) {
		if (arg0 == null)
			return false;
		Path<KIMVertex, KIMEdge> p = (Path<KIMVertex, KIMEdge>) arg0;
		if (!p.getCost().equals(cost))
			return false;
		if (p.getVertices().size() != vertices.size())
			return false;
		int i = 0;
		for (; i < p.getEdges().size()
				&& p.getEdges().get(i).equals(edges.get(i)); i++)
			;
		return i == p.getEdges().size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getCost()
	 */
	public Double getCost() {
		return cost;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getDevNode()
	 */
	public KIMVertex getDevVertex() {
		return vertices.get(devNodeIndex);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getDevNodeIndex()
	 */
	public int getDevNodeIndex() {
		return devNodeIndex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getEdge(int)
	 */
	public KIMEdge getEdge(int pos) {
		return edges.get(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getEdges()
	 */
	public List<KIMEdge> getEdges() {
		return edges;
	}

	public int getIteracao() {
		return iteracao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getOrdem()
	 */
	public int getOrdem() {
		return ordem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getOrigem()
	 */
	public String getOrigem() {
		return origem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getParent()
	 */
	public Path<KIMVertex, KIMEdge> getParent() {
		return parent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getPrefix(edu
	 * .uci.ics.jung.algorithms.shortestpath.KIMVertex)
	 */
	public Path<KIMVertex, KIMEdge> getPrefix(KIMVertex last) {
		KIMVertex f = this.getStart();
		int j = 0;
		for (KIMEdge atual : edges) {
			j++;
			f = g.getOpposite(f, atual);
			if (f.equals(last))
				break;
		}
		List<KIMEdge> newEdges = edges.subList(0, j);
		return new BasicPath<KIMVertex, KIMEdge>(newEdges, this.getStart(), g,
				nev);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getReverse()
	 */
	public Path<KIMVertex, KIMEdge> getReverse() {
		Iterator<KIMEdge> i = edges.iterator();
		ArrayList<KIMEdge> reversedEdges = new ArrayList<KIMEdge>(edges.size());
		while (i.hasNext()) {
			KIMEdge atual = i.next();
			reversedEdges.add(0, atual);
		}
		return new BasicPath<KIMVertex, KIMEdge>(reversedEdges, vertices
				.get(vertices.size() - 1), g, nev);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getStart()
	 */
	public KIMVertex getStart() {
		return vertices.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getSubPath(edu
	 * .uci.ics.jung.algorithms.shortestpath.KIMVertex)
	 */
	public Path<KIMVertex, KIMEdge> getSubPath(KIMVertex v) {
		Iterator<KIMEdge> i = edges.iterator();
		KIMVertex f = this.getStart();
		int j = 0;
		while (i.hasNext()) {
			j++;
			KIMEdge atual = i.next();
			f = g.getOpposite(f, atual);
			if (f.equals(v))
				break;

		}
		List<KIMEdge> newEdges = edges.subList(j, edges.size());
		return new BasicPath<KIMVertex, KIMEdge>(newEdges, v, g, nev);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getSubPath(edu
	 * .uci.ics.jung.algorithms.shortestpath.KIMVertex, int, int)
	 */
	public Path<KIMVertex, KIMEdge> getSubPath(KIMVertex v, int from, int to) {
		return new BasicPath<KIMVertex, KIMEdge>(edges.subList(from, to), v, g,
				nev);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getTarget()
	 */
	public KIMVertex getTarget() {
		return vertices.get(vertices.size() - 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getVertex(int)
	 */
	public KIMVertex getVertex(int pos) {
		return vertices.get(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#getVertices()
	 */
	public List<KIMVertex> getVertices() {
		return vertices;
	}

	public void setIteracao(int iteracao) {
		this.iteracao = iteracao;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#setOrdem(int)
	 */
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#setOrigem(java
	 * .lang.String)
	 */
	public void setOrigem(String origem) {
		this.origem = origem;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#setParent(edu
	 * .uci.ics.jung.algorithms.shortestpaths.PathInterface)
	 */
	public void setParent(Path<KIMVertex, KIMEdge> parent) {
		this.parent = parent;
		computeDeviation();
		if (devNodeIndex == null)
			throw new NullPointerException();
	}

	private void setVertices(KIMVertex s) {
		vertices = new Vector<KIMVertex>();
		Iterator<KIMEdge> e = edges.iterator();
		KIMEdge atual = null;
		KIMVertex p = s;
		vertices.add(s);
		cost = 0;
		while (e.hasNext()) {
			atual = e.next();
			cost += nev.transform(atual).doubleValue();
			vertices.add((KIMVertex) g.getOpposite(p, atual));
			p = (KIMVertex) g.getOpposite(p, atual);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.uci.ics.jung.algorithms.shortestpaths.PathInterface#toString()
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		int ordem = -1;
		if (parent != null)
			ordem = parent.getOrdem();
		sb.append("<Path par=" + ordem + " cost=" + cost + " devPos="
				+ devNodeIndex + " origem=" + origem + " it=" + iteracao + ">");
		sb.append(vertices.toString());
		// sb.append("\n<Parent>\n");
		// sb.append(parent);
		// sb.append("\n</Parent>\n");
		sb.append("</Path>");
		return sb.toString();
	}
}
