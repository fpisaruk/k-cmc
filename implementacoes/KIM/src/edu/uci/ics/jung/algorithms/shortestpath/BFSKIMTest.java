package edu.uci.ics.jung.algorithms.shortestpath;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.commons.collections15.functors.ConstantTransformer;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM.TreeType;
import edu.uci.ics.jung.algorithms.transformation.UndirectGraphToTree;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.utils.draw.KIMGraphDraw;

public class BFSKIMTest {
	static Logger logger = Logger.getLogger(BFSKIMTest.class);

	BidiMap<KIMVertex, String> mapVertex = new DualHashBidiMap<KIMVertex, String>();
	BidiMap<KIMEdge, Integer> mapEdge = new DualHashBidiMap<KIMEdge, Integer>();
	KIMVertex a = new KIMVertex("a");
	KIMVertex b = new KIMVertex("b");
	KIMVertex c = new KIMVertex("c");
	KIMVertex d = new KIMVertex("d");
	KIMVertex e = new KIMVertex("e");

	private UndirectedGraph<KIMVertex, KIMEdge> getGrafo() {
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		// Insere o caminho a,c,e
		a.setIsOnPath(true);
		c.setIsOnPath(true);
		e.setIsOnPath(true);
		a.setEpsilon(1);
		c.setEpsilon(2);
		e.setEpsilon(3);
		a.setZeta(1);
		c.setZeta(2);
		e.setZeta(3);
		g.addVertex(a);
		g.addVertex(b);
		g.addVertex(c);
		g.addVertex(d);
		g.addVertex(e);
		g.addEdge(new KIMEdge(), a, b);
		g.addEdge(new KIMEdge(), a, c);
		g.addEdge(new KIMEdge(), a, d);
		g.addEdge(new KIMEdge(), b, c);
		g.addEdge(new KIMEdge(), b, e);
		g.addEdge(new KIMEdge(), c, e);
		g.addEdge(new KIMEdge(), c, d);
		g.addEdge(new KIMEdge(), d, e);

		for (KIMVertex vertex : g.getVertices())
			mapVertex.put(vertex, vertex.getNome());
		for (KIMEdge edge : g.getEdges())
			mapEdge.put(edge, edge.getID());

		return g;
	}

	@Test
	public void testGetIncomingEdge() {
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		BFSKIM<KIMVertex, KIMEdge> bfs = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TS);
		KIMEdge edge = bfs.getIncomingEdge(mapVertex.getKey("a"), mapVertex
				.getKey("e"));
		Assert.assertTrue(mapEdge.getKey(6).equals(edge));
		edge = bfs
				.getIncomingEdge(mapVertex.getKey("a"), mapVertex.getKey("c"));
		Assert.assertTrue(mapEdge.getKey(2).equals(edge));
	}

	@Test
	public void testGetPath() {
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		BFSKIM<KIMVertex, KIMEdge> bfsTS = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TS);
		List<KIMEdge> p = bfsTS.getPath(a, e);

		Assert.assertTrue(g.getOpposite(a, p.get(0)).equals(c));
		Assert.assertTrue(g.getOpposite(c, p.get(1)).equals(e));

		Assert.assertEquals(a.getEpsilon(), 1, 0);
		Assert.assertEquals(b.getEpsilon(), 1, 0);
		Assert.assertEquals(c.getEpsilon(), 2, 0);
		Assert.assertEquals(d.getEpsilon(), 1, 0);
		Assert.assertEquals(e.getEpsilon(), 3, 0);

		BFSKIM<KIMVertex, KIMEdge> bfsTT = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TT);
		List<KIMEdge> rp = bfsTT.getPath(e, a);
		Assert.assertTrue(g.getOpposite(e, rp.get(0)).equals(c));
		Assert.assertTrue(g.getOpposite(c, rp.get(1)).equals(a));
		Assert.assertEquals(a.getZeta(), 1, 0);
		Assert.assertEquals(b.getZeta(), 3, 0);
		Assert.assertEquals(c.getZeta(), 2, 0);
		Assert.assertEquals(d.getZeta(), 3, 0);
		Assert.assertEquals(e.getZeta(), 3, 0);

		Tree<KIMVertex, KIMEdge> TS = new UndirectGraphToTree(a, bfsTS
				.getIncomingEdgeMap(a)).transform(g);
		Tree<KIMVertex, KIMEdge> TT = new UndirectGraphToTree(e, bfsTT
				.getIncomingEdgeMap(e)).transform(g);

		KIMGraphDraw.desenha(g, TS, TT, KIMEdge.getDefautTransformer());
	}

	@Test
	public void testGetSons() {
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		BFSKIM<KIMVertex, KIMEdge> bfsTS = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TS);
		Assert.assertTrue(bfsTS.getSons(a, a).contains(b));
		Assert.assertTrue(bfsTS.getSons(a, a).contains(c));
		Assert.assertTrue(bfsTS.getSons(a, a).contains(d));
		Assert.assertTrue(bfsTS.getSons(a, c).contains(e));
		BFSKIM<KIMVertex, KIMEdge> bfsTT = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TT);
		Assert.assertTrue(bfsTT.getSons(e, e).contains(b));
		Assert.assertTrue(bfsTT.getSons(e, e).contains(c));
		Assert.assertTrue(bfsTT.getSons(e, e).contains(d));
		Assert.assertTrue(bfsTT.getSons(e, c).contains(a));
	}

	@Test
	public void testGetIncomingEdgeMap() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetDistance() {
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		BFSKIM<KIMVertex, KIMEdge> bfsTS = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TS);
		Assert.assertEquals(bfsTS.getDistance(a, a), 0);
		Assert.assertEquals(bfsTS.getDistance(a, b), 1);
		Assert.assertEquals(bfsTS.getDistance(a, c), 1);
		Assert.assertEquals(bfsTS.getDistance(a, d), 1);
		Assert.assertEquals(bfsTS.getDistance(a, e), 2);
		BFSKIM<KIMVertex, KIMEdge> bfsTT = new BFSKIM<KIMVertex, KIMEdge>(g,
				ShortestPathKIM.TreeType.TT);
		Assert.assertEquals(bfsTT.getDistance(e, a), 2);
		Assert.assertEquals(bfsTT.getDistance(e, b), 1);
		Assert.assertEquals(bfsTT.getDistance(e, c), 1);
		Assert.assertEquals(bfsTT.getDistance(e, d), 1);
		Assert.assertEquals(bfsTT.getDistance(e, e), 0);
	}

	@Test
	public void testGetDistanceMap() throws IOException {
		logger.debug("<testGetDistanceMap>");
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer nev = new ConstantTransformer<Number>(null);
		g = pnr.load("data/pajNetTest.dat", g);
		Iterator<KIMVertex> i = g.getVertices().iterator();
		BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			atual.setNome(pnr.getVertexLabeller().transform(atual));
			bMap.put(atual, atual.getNome());
			logger.debug(atual + " -> ("
					+ pnr.getVertexLabeller().transform(atual) + ")");
		}
		Iterator<KIMEdge> j = g.getEdges().iterator();
		while (j.hasNext()) {
			KIMEdge atual = j.next();
			logger.debug(atual + " -> " + nev.transform(atual));
		}
		KIMVertex start = bMap.getKey("a");
		KIMVertex target = bMap.getKey("i");
		BFSKIM<KIMVertex, KIMEdge> bfs = new BFSKIM<KIMVertex, KIMEdge>(g);
		Map<KIMVertex, Number> dMap = bfs.getDistanceMap(start);
		logger.info(dMap);
		Assert.assertEquals(dMap.get(bMap.getKey("a")), 0);
		Assert.assertEquals(dMap.get(bMap.getKey("b")), 1);
		Assert.assertEquals(dMap.get(bMap.getKey("c")), 2);
		Assert.assertEquals(dMap.get(bMap.getKey("d")), 3);
		Assert.assertEquals(dMap.get(bMap.getKey("e")), 2);
		Assert.assertEquals(dMap.get(bMap.getKey("f")), 3);
		Assert.assertEquals(dMap.get(bMap.getKey("g")), 4);
		Assert.assertEquals(dMap.get(bMap.getKey("h")), 3);
		Assert.assertEquals(dMap.get(bMap.getKey("i")), 5);

		bfs = new BFSKIM<KIMVertex, KIMEdge>(g);
		dMap = bfs.getDistanceMap(target);
		logger.info(dMap);
		Assert.assertEquals(dMap.get(bMap.getKey("a")), 5);
		Assert.assertEquals(dMap.get(bMap.getKey("b")), 4);
		Assert.assertEquals(dMap.get(bMap.getKey("c")), 3);
		Assert.assertEquals(dMap.get(bMap.getKey("d")), 2);
		Assert.assertEquals(dMap.get(bMap.getKey("e")), 3);
		Assert.assertEquals(dMap.get(bMap.getKey("f")), 2);
		Assert.assertEquals(dMap.get(bMap.getKey("g")), 1);
		Assert.assertEquals(dMap.get(bMap.getKey("h")), 2);
		Assert.assertEquals(dMap.get(bMap.getKey("i")), 0);

		// MyGraphDraw.desenha(g, nev);
		logger.debug("</testGetDistanceMap>");
	}
}
