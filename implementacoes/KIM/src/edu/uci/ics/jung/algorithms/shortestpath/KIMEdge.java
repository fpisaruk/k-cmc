package edu.uci.ics.jung.algorithms.shortestpath;

import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;

public class KIMEdge {
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		try {
			return ((KIMEdge) obj).getID().equals(myID);
		} catch (ClassCastException e) {
			return false;

		}
	}

	private static int id;
	private static Transformer<KIMEdge, Number> defaultCost = new Transformer<KIMEdge, Number>() {
		public Number transform(KIMEdge link) {
			return link.getCost();
		}
	};
	private static Transformer<KIMEdge, Number> constantCost = new Transformer<KIMEdge, Number>() {
		public Number transform(KIMEdge link) {
			return 1;
		}
	};
	private int myID;

	@Override
	public String toString() {
		return "[E" + myID + "," + cost + "]";
	}

	private Number getCost() {
		return cost;
	}

	public void setCost(Number cost) {
		this.cost = cost;
	}

	public Integer getID() {
		return myID;
	}

	private Number cost;

	public KIMEdge() {
		this(null);
	};

	public KIMEdge(Number cost) {
		synchronized (KIMEdge.class) {
			id++;
			this.myID = id;
		}
		this.cost = cost;
	}

	public static final Transformer<KIMEdge, Number> getDefautTransformer() {
		return defaultCost;
	}

	public static final Transformer<KIMEdge, Number> getConstantTransformer() {
		return constantCost;
	}

	public static class KIMEdgeFactory implements Factory<KIMEdge> {
		@Override
		public KIMEdge create() {
			return new KIMEdge();
		}
	}

	public static Factory<KIMEdge> getFactory() {
		return new KIMEdge.KIMEdgeFactory();
	}
}