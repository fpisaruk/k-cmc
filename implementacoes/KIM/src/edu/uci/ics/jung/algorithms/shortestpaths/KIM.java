package edu.uci.ics.jung.algorithms.shortestpaths;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;

import edu.uci.ics.jung.algorithms.generators.random.ConnectedUndirectedGraphGenerator;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.utils.PathsToTree;
import edu.uci.ics.jung.utils.StopWatch;
import edu.uci.ics.jung.utils.draw.KIMGraphDraw;
import edu.uci.ics.jung.utils.draw.SimpleGraphDraw;
import edu.uci.ics.jung.utils.draw.SimpleTreeDraw;

/**
 * 
 * @author Fábio Pisaruk
 * 
 */
public class KIM<V extends KIMVertex, E extends KIMEdge> implements
		ShortestSimplePaths<V> {

	static Logger logger = Logger.getLogger(KIM.class);

	public static void main(String[] args) throws IOException {
		if (1 == 1) {
			PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
					KIMVertex.getFactory(), KIMEdge.getFactory());
			UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
			// Transformer<KIMEdge, Number> nev =
			// pnr.getEdgeWeightTransformer();
			g = pnr.load("data/pajNetTest.dat", g);
			// Path.init(nev, g);
			Iterator<KIMVertex> i = g.getVertices().iterator();
			BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();
			while (i.hasNext()) {
				KIMVertex atual = i.next();
				atual.setNome(pnr.getVertexLabeller().transform(atual));
				bMap.put(atual, atual.getNome());
			}
			KIMVertex start = bMap.getKey("a");
			KIMVertex target = bMap.getKey("i");
			int k = 700;
			KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, pnr
					.getEdgeWeightTransformer(),
					ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
			Path<KIMVertex, KIMEdge> paths[] = kim.getPaths(start, target, k);
			for (int h = 0; h < k && paths[h] != null; h++)
				logger.debug(paths[h]);
			logger.debug(kim.getStopWatchPerPath().prettyPrint());
			List<Path<KIMVertex, KIMEdge>> lp = new ArrayList<Path<KIMVertex, KIMEdge>>();
			Tree<KIMVertex, KIMEdge> tree = new PathsToTree(pnr
					.getEdgeWeightTransformer())
					.transform(Arrays.asList(paths));
			SimpleTreeDraw.desenha(tree, KIMEdge.getDefautTransformer(),
					"Todos os caminhos", false);
		} else {
			ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge> gerador = new ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge>(
					100, 0.1);
			UndirectedGraph<KIMVertex, KIMEdge> graph = (UndirectedGraph<KIMVertex, KIMEdge>) gerador
					.create();
			ConnectedUndirectedGraphGenerator.atribuiCustosNasArestas(graph,
					Double.MIN_VALUE, Double.MAX_VALUE
							/ (graph.getEdgeCount() + 1));
			List<KIMVertex> vertices = new ArrayList<KIMVertex>(graph
					.getVertices());
			for (int i = 0; i < vertices.size(); i++)
				logger.debug("V" + vertices.get(i));
			Random rand = new Random();
			KIMVertex start = vertices.get(rand.nextInt(vertices.size()));
			KIMVertex target = vertices.get(rand.nextInt(vertices.size()));
			KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(graph,
					KIMEdge.getDefautTransformer(),
					ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
			int k = 20;
			Path<KIMVertex, KIMEdge> paths[] = kim.getPaths(start, target, k);
			for (int h = 0; h < k; h++)
				logger.info(paths[h]);
			logger.info(kim.getStopWatchPerPath().prettyPrint());
			logger.info(kim.getStopWatchKIM().shortSummary());
			logger.info("Average time for " + (k + 1) + "-paths = "
					+ kim.getStopWatchKIM().getTotalTimeMillis() / k + " ms.");
		}
	}

	private long avgMem = 0;

	private StopWatch stopWatchPerPath = new StopWatch("Tempo por caminho");
	private StopWatch stopWatchSEP = new StopWatch();
	private StopWatch stopWatchTrees = new StopWatch();

	private UndirectedGraph<KIMVertex, KIMEdge> graph;
	// private UndirectedGraph<KIMVertex, KIMEdge> grafoOriginal;
	private Transformer<KIMEdge, Number> nev;

	private Path.Factory pathFactory;

	private ShortestPathKIM.Factory spKIMFactory;
	private StopWatch stopWatchKIM = new StopWatch();

	private Path<KIMVertex, KIMEdge>[] bestPaths;

	private SortedSet<Path<KIMVertex, KIMEdge>> candidatePaths;

	private TreeSet<Integer>[] W;

	private DeviationArcsMap<KIMEdge>[] deviationArcs;

	private boolean desenhar = false;

	public KIM(UndirectedGraph<KIMVertex, KIMEdge> graph) {
		this(graph, null, ShortestPathKIM.BFSShortestPathKIMAlgorithm);
	}

	public KIM(UndirectedGraph<KIMVertex, KIMEdge> graph,
			Transformer<KIMEdge, Number> nev,
			Class<ShortestPathKIM<KIMVertex, KIMEdge>> spAlg) {
		this.graph = graph;
		if (nev == null)
			this.nev = KIMEdge.getConstantTransformer();
		else
			this.nev = nev;
		pathFactory = new Path.Factory(graph, this.nev);
		spKIMFactory = new ShortestPathKIM.Factory(this.nev, graph, spAlg);
	}

	protected Path<KIMVertex, KIMEdge> FSP(KIMVertex s, KIMVertex t,
			Path<KIMVertex, KIMEdge> R) {
		logger.debug("<FSP s=" + s + " t=" + t + " R " + R + ">");

		ShortestPathKIM<KIMVertex, KIMEdge> Ts = spKIMFactory
				.newInstance(ShortestPathKIM.TreeType.TS);
		ShortestPathKIM<KIMVertex, KIMEdge> Tt = spKIMFactory
				.newInstance(ShortestPathKIM.TreeType.TT);

		stopWatchTrees.start("Trees");
		Ts.getIncomingEdgeMap(s);
		Tt.getIncomingEdgeMap(t);
		stopWatchTrees.stop();
		int alfa = R.getVertices().size();
		Pair<?> resp = SEP(Ts, Tt, s, t, alfa);

		if (desenhar)
			KIMGraphDraw.desenha(graph, Ts, s, Tt, t, nev, bestPaths);

		if (resp == null)
			return null;

		if (resp.getSecond().equals(resp.getFirst()))
			logger.debug("Caminho Tipo I");
		else
			logger.debug("Caminho Tipo II");
		Path<KIMVertex, KIMEdge> path = pathFactory.newInstance(Ts, Tt, resp,
				s, t);
		logger.debug("Caminho Passado = " + R);
		logger.debug("Caminho gerado = " + path);

		logger.debug("</FSP>");
		return path;
	}

	public long getAvgMem() {
		return avgMem;
	}

	/**
	 * Retorna o maior elemento de <i>w</i> menor ou igual a <i>alfaK - 1</i> e
	 * maior ou igual a <i>alfaJ</i>.
	 * <p>
	 * delta := max{x E w | alfaJ =< x < alfaK}
	 * </p>
	 * 
	 * @param w
	 * @param alfaJ
	 * @param alfaK
	 * @return
	 */
	private int getDelta(TreeSet<Integer> w, int alfaJ, int alfaK) {
		logger.debug("computeDelta Wj=" + w + " alfaK=" + alfaK + " AlfaJ="
				+ alfaJ);
		Integer delta = w.floor(alfaK - 1);
		logger.debug("delta calculado=" + delta);
		if (delta == null || delta < alfaJ + 1)
			return Integer.MIN_VALUE;
		return delta;
	}

	/**
	 * Retorna o menor elemento de <i>w</i> que seja maior que <i>alfaK</i> e
	 * menor ou igual a <i>qj</i>
	 * <p>
	 * gama := min{x E w | alfaK < x <= qj}
	 * </p>
	 * 
	 * @param w
	 * @param alfaK
	 * @param qj
	 * @return
	 */
	private int getGama(TreeSet<Integer> w, int alfaK, int qj) {
		logger.debug("computeGama Wk=" + w + " alfaK=" + alfaK + " qj=" + qj);
		Integer gama = w.ceiling(alfaK + 1);
		if (gama == null || gama > qj)
			return Integer.MIN_VALUE;
		return gama;
	}

	protected Path<KIMVertex, KIMEdge> getPa(KIMVertex source,
			KIMVertex target, Path<KIMVertex, KIMEdge> pk) {
		logger.debug("<Pa source=" + source + " target=" + target + " pk= "
				+ pk + " >");
		Path<KIMVertex, KIMEdge> resp = null;
		KIMVertex start = pk.getVertices().get(pk.getDevNodeIndex() + 1);
		Path<KIMVertex, KIMEdge> p = pk.getSubPath(start);
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> verticesToRestore = removeVertices(pk
				.getVertices().subList(0, pk.getDevNodeIndex() + 1));
		insertPathOnTree(pk, pk.getDevNodeIndex() + 1);
		desenharGrafo("Grafo para calculo de Pa");
		Path<KIMVertex, KIMEdge> pa = FSP(start, target, p);
		removePathFromTree(pk, pk.getDevNodeIndex() + 1);
		restoreVertices(pk.getVertices().subList(0, pk.getDevNodeIndex() + 1),
				verticesToRestore);
		if (pa != null) {
			resp = pathFactory.newInstance(pk.getPrefix(start), pa);
			resp.setParent(pk);
			resp.setOrigem("A");
			desenhaArvoreDeCaminhos(resp);
		}
		logger.debug("Pa = " + resp);
		logger.debug("</Pa>");
		return resp;
	}

	private void desenharGrafo(String title) {
		if (!desenhar)
			return;
		SimpleGraphDraw.desenha(graph, nev, title);
	}

	private void desenhaArvoreDeCaminhos(Path<KIMVertex, KIMEdge> resp) {
		if (!desenhar)
			return;
		Set<Path<KIMVertex, KIMEdge>> paths = new HashSet<Path<KIMVertex, KIMEdge>>();
		CollectionUtils.addAll(paths, candidatePaths.iterator());
		paths.add(resp);
		for (int i = 0; i < bestPaths.length && bestPaths[i] != null; i++)
			paths.add(bestPaths[i]);
		Tree<KIMVertex, KIMEdge> tree = new PathsToTree(nev).transform(paths);
		SimpleTreeDraw.desenha(tree, KIMEdge.getDefautTransformer(),
				"Caminho P" + resp.getOrigem(), true);
	}

	@Override
	public Path<KIMVertex, KIMEdge>[] getPaths(KIMVertex source,
			KIMVertex target, int nOfPaths) {
		avgMem = 0;
		long initMem = getUsedMemory();
		stopWatchKIM.start();
		stopWatchPerPath.start("0");
		int k = 0, gama, delta;
		W = new TreeSet[nOfPaths];
		deviationArcs = new DeviationArcsMap[nOfPaths];
		candidatePaths = new TreeSet<Path<KIMVertex, KIMEdge>>();
		bestPaths = new Path[nOfPaths];
		DijkstraShortestPath<KIMVertex, KIMEdge> dsp = new DijkstraShortestPath<KIMVertex, KIMEdge>(
				graph, nev);
		Path<KIMVertex, KIMEdge> p1 = pathFactory.newInstance(dsp.getPath(
				source, target), source);
		stopWatchPerPath.stop();
		avgMem += getUsedMemory() - initMem;
		initMem = getUsedMemory();
		bestPaths[k] = p1;
		if (p1 == null) {
			stopWatchKIM.stop();
			return bestPaths;
		}
		p1.setIteracao(0);
		p1.setParent(null);
		p1.setOrdem(0);
		W[0] = new TreeSet<Integer>();
		W[0].add(0);
		W[0].add(p1.getVertices().size() - 1);
		logger.info("1 º path = " + p1);
		if (nOfPaths == 1) {
			stopWatchKIM.stop();
			return bestPaths;
		}
		stopWatchPerPath.start("1");
		insertPathOnTree(p1, 0);
		Path<KIMVertex, KIMEdge> p2 = FSP(source, target, p1);
		removePathFromTree(p1, 0);
		if (p2 == null) {
			stopWatchKIM.stop();
			return bestPaths;
		}
		p2.setParent(p1);
		p2.setIteracao(0);
		candidatePaths.add(p2);
		deviationArcs[0] = new DeviationArcsMap<KIMEdge>(p1.getVertices()
				.size());
		while (!candidatePaths.isEmpty()) {
			k++;
			Path<KIMVertex, KIMEdge> pk = candidatePaths.first();
			pk.setOrdem(k);
			logger.info((k + 1) + " º path = " + pk);
			if (k == 30) {
				System.out.println("27");
				// desenhar=true;
			} else
				desenhar = false;
			bestPaths[k] = pk;
			stopWatchPerPath.stop();
			avgMem += getUsedMemory() - initMem;
			validaCaminhos(k);
			if (k == nOfPaths - 1) {
				logger.debug("ACABOU DE ACHAR TODOS OS CAMINHOS PEDIDOS");
				break;
			}
			logger.debug("Candidatos =" + candidatePaths);
			stopWatchPerPath.start(Integer.toString(k + 1));
			initMem = getUsedMemory();
			candidatePaths.remove(pk);
			Path<KIMVertex, KIMEdge> pj = pk.getParent();
			Path<KIMVertex, KIMEdge> pa = null;
			if (pk.getDevNodeIndex() + 1 != pk.getVertices().size() - 1) {
				pa = getPa(source, target, pk);
				if (pa != null) {
					// pa.setParent(pk);
					// pa.setOrigem("A");
					logger.debug("Pa=" + pa);
					logger.debug(candidatePaths);
					logger.debug(bestPaths);
					if (candidatePaths.contains(pa))
						throw new RuntimeException("Caminho já existe " + pa);
					pa.setIteracao(k);
					candidatePaths.add(pa);
				}
			}
			W[k] = new TreeSet<Integer>();
			W[k].add(pk.getDevNodeIndex() + 1);
			W[k].add(pk.getVertices().size() - 1);
			deviationArcs[k] = new DeviationArcsMap<KIMEdge>(pk.getVertices()
					.size());
			deviationArcs[pj.getOrdem()].put(pk.getDevNodeIndex(), pk
					.getEdge(pk.getDevNodeIndex()));
			gama = getGama(W[pj.getOrdem()], pk.getDevNodeIndex(), pj
					.getVertices().size() - 1);
			Path<KIMVertex, KIMEdge> pb = null;
			if (gama >= 0)
				pb = getPb(deviationArcs[pj.getOrdem()], source, target, pk,
						pj, gama);
			if (pb != null) {
				// pb.setParent(pk.getParent());
				// pb.setOrigem("B");
				logger.debug("Pb=" + pb);
				if (candidatePaths.contains(pb))
					throw new RuntimeException("Caminho já existe!");
				pb.setIteracao(k);
				candidatePaths.add(pb);
			}
			logger.debug("W=" + W[pj.getOrdem()]);
			if (!W[pj.getOrdem()].contains(pk.getDevNodeIndex())) {
				W[pj.getOrdem()].add(pk.getDevNodeIndex());
				delta = getDelta(W[pj.getOrdem()], pj.getDevNodeIndex(), pk
						.getDevNodeIndex());
				Path<KIMVertex, KIMEdge> pc = null;
				if (delta >= 0)
					pc = getPc(deviationArcs[pj.getOrdem()], source, target,
							pk, pj, delta);
				if (pc != null) {
					// pc.setParent(pk.getParent());
					// pc.setOrigem("C");
					logger.debug("Pc=" + pc);
					if (candidatePaths.contains(pc))
						throw new RuntimeException("Caminho já existe");
					pc.setIteracao(k);
					candidatePaths.add(pc);
				}
			}
		}
		stopWatchKIM.stop();
		avgMem /= k;
		logger.info(stopWatchKIM.shortSummary());
		logger.info("Average memoryUsage = " + avgMem);
		logger.info("Average time for " + k + "-paths = "
				+ stopWatchKIM.getTotalTimeMillis() / k + " ms.");
		return bestPaths;
	}

	private void validaCaminhos(int k) {
		if (bestPaths[k].getCost() < bestPaths[k - 1].getCost()) {
			Iterator<KIMEdge> i = bestPaths[k].getEdges().iterator();
			double soma = 0;
			while (i.hasNext()) {
				KIMEdge edge = i.next();
				System.out.println("CUSTO =" + nev.transform(edge));
				soma += nev.transform(edge).doubleValue();
			}
			System.out.println("SOMA=" + soma);
			throw new RuntimeException(
					"ESTA GERANDO CAMINHOS COM CUSTOS MENORES????");
		}
	}

	protected Path<KIMVertex, KIMEdge> getPb(
			DeviationArcsMap<KIMEdge> devArcsMap, KIMVertex source,
			KIMVertex target, Path<KIMVertex, KIMEdge> pk,
			Path<KIMVertex, KIMEdge> pj, int gama) {
		logger.debug("<Pb pk=" + pk + " pj=" + pj + " gama=" + gama + "/>");
		Path<KIMVertex, KIMEdge> R = pj.getSubPath(pk.getDevVertex(), pk
				.getDevNodeIndex(), gama);
		logger.debug("R = " + R);
		logger.debug("Arcos a serem removidos = "
				+ getArcos(devArcsMap.get(pk.getDevNodeIndex())));
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore = removeEdges(devArcsMap
				.get(pk.getDevNodeIndex()));
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> verticesToRestore = removeVertices(pj
				.getVertices().subList(0, pk.getDevNodeIndex()));
		insertPathOnTree(pj, pk.getDevNodeIndex());
		desenharGrafo("Grafo para calculo de Pb");
		Path<KIMVertex, KIMEdge> pb = FSP(pk.getVertex(pk.getDevNodeIndex()),
				target, R);
		removePathFromTree(pj, pk.getDevNodeIndex());
		logger.debug("Pb = " + pb);
		Path<KIMVertex, KIMEdge> resp = null;
		restoreVertices(pj.getVertices().subList(0, pk.getDevNodeIndex()),
				verticesToRestore);
		restoreEdges(arcsToRestore);
		if (pb != null) {
			resp = pathFactory.newInstance(pj.getSubPath(pj.getStart(), 0, pk
					.getDevNodeIndex()), pb);
			resp.setParent(pk.getParent());
			resp.setOrigem("B");
			desenhaArvoreDeCaminhos(resp);
		}
		logger.debug("Resp=" + resp);

		logger.debug("</Pb>");
		return resp;
	}

	private String getArcos(Set<KIMEdge> edges) {
		StringBuffer sb = new StringBuffer();
		if (edges == null)
			return "";
		for (KIMEdge edge : edges) {
			sb.append(graph.getEndpoints(edge) + " ");
		}
		return sb.toString();
	}

	protected Path<KIMVertex, KIMEdge> getPc(
			DeviationArcsMap<KIMEdge> devArcsMap, KIMVertex source,
			KIMVertex target, Path<KIMVertex, KIMEdge> pk,
			Path<KIMVertex, KIMEdge> pj, int lambda) {
		logger.debug("<Pc pk=" + pk + " pj=" + pj + "/>");
		Path<KIMVertex, KIMEdge> R = pj.getSubPath(pj.getVertex(lambda),
				lambda, pk.getDevNodeIndex());
		logger.debug("R = " + R);
		logger.debug("Arcos a serem removidos = "
				+ getArcos(devArcsMap.get(lambda)));
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore = removeEdges(devArcsMap
				.get(lambda));
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> verticesToRestore = removeVertices(pj
				.getVertices().subList(0, lambda));
		insertPathOnTree(pj, lambda);
		desenharGrafo("Grafo para calculo de Pc");
		Path<KIMVertex, KIMEdge> pc = FSP(pj.getVertex(lambda), target, R);
		removePathFromTree(pj, lambda);
		logger.debug("Pc=" + pc);
		Path<KIMVertex, KIMEdge> resp = null;
		restoreVertices(pj.getVertices().subList(0, lambda), verticesToRestore);
		restoreEdges(arcsToRestore);
		if (pc != null) {
			resp = pathFactory.newInstance(pj.getSubPath(pj.getStart(), 0,
					lambda), pc);
			resp.setParent(pk.getParent());
			resp.setOrigem("C");
			desenhaArvoreDeCaminhos(resp);
		}
		logger.debug("Resp=" + resp);
		logger.debug("</Pc>");
		return resp;
	}

	private ShortestPathKIM<KIMVertex, KIMEdge> getShortestPathAlgorithm(
			ShortestPathKIM.TreeType treeType) {
		try {
			return spKIMFactory.newInstance(treeType);
		} catch (Exception e) {
			System.err.println(e);
			e.printStackTrace(System.err);
			RuntimeException err = new RuntimeException(e.getCause());
			throw err;
		}
	}

	protected Set<KIMVertex> getSons(ShortestPathKIM<KIMVertex, KIMEdge> T,
			KIMVertex u, KIMVertex s) {
		return T.getSons(s, u);
		/*
		 * Iterator<KIMEdge> i = graph.getOutEdges(u) .iterator();
		 * Set<KIMVertex> sons = new HashSet<KIMVertex>(); while (i.hasNext()) {
		 * KIMEdge atual = i.next(); KIMVertex o = graph.getOpposite(u, atual);
		 * KIMEdge incident = T.getIncomingEdge(s, o); if (incident != null &&
		 * graph.getOpposite(o, incident) .equals(u)) sons.add(o); } return
		 * sons;
		 */
	}

	public StopWatch getStopWatchKIM() {
		return stopWatchKIM;
	}

	public StopWatch getStopWatchPerPath() {
		return stopWatchPerPath;
	}

	public StopWatch getStopWatchSEP() {
		return stopWatchSEP;
	}

	public StopWatch getStopWatchTrees() {
		return stopWatchTrees;
	}

	private long getUsedMemory() {
		return Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory();
	}

	public boolean graphsAreEqual(UndirectedGraph<KIMVertex, KIMEdge> g1,
			UndirectedGraph<KIMVertex, KIMEdge> g2) {
		if (g1.getVertices().size() != g2.getVertices().size())
			return false;
		if (g1.getEdges().size() != g2.getEdges().size())
			return false;
		Iterator<KIMEdge> iv = g1.getEdges().iterator();
		while (iv.hasNext()) {
			KIMEdge edge = iv.next();
			KIMVertex pa = g1.getEndpoints(edge).getFirst();
			KIMVertex pb = g1.getEndpoints(edge).getSecond();
			if (!g2.findEdge(pa, pb).equals(edge))
				return false;
		}
		return true;
	}

	protected void insertPathOnTree(Path<KIMVertex, KIMEdge> path, int ini) {
		for (int i = ini, epsilon = 1; i < path.getVertices().size(); i++, epsilon++) {
			KIMVertex atual = path.getVertex(i);
			atual.setIsOnPath(true);
			atual.setEpsilon(epsilon);
			atual.setZeta(epsilon);
		}
	}

	/*
	 * protected void insertPath(Path<KIMVertex, KIMEdge> path, int ini, int
	 * fim) { for (int i = ini, epsilon = 1; i < fim; i++, epsilon++) {
	 * KIMVertex atual = path.getVertex(i); atual.setIsOnPath(true);
	 * atual.setEpsilon(epsilon); atual.setZeta(epsilon); } }
	 */

	private LinkedHashMap<KIMEdge, Pair<KIMVertex>> removeEdges(
			Set<KIMEdge> list) {
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore = new LinkedHashMap<KIMEdge, Pair<KIMVertex>>();
		if (list != null) {
			for (KIMEdge eCur : list) {
				Pair<KIMVertex> pontas = graph.getEndpoints(eCur);
				arcsToRestore.put(eCur, pontas);
				graph.removeEdge(eCur);
			}
		}
		return arcsToRestore;
	}

	private void removePathFromTree(Path<KIMVertex, KIMEdge> path, int ini) {
		for (int i = ini; i < path.getVertices().size(); i++) {
			KIMVertex atual = path.getVertex(i);
			atual.setIsOnPath(false);
			atual.setEpsilon(Integer.MAX_VALUE);
			atual.setZeta(Integer.MIN_VALUE);
		}
	}

	/*
	 * private void removePath(Path<KIMVertex, KIMEdge> path, int ini, int fim)
	 * { for (int i = ini; i < fim; i++) { KIMVertex atual = path.getVertex(i);
	 * atual.setIsOnPath(false); atual.setEpsilon(Integer.MAX_VALUE);
	 * atual.setZeta(Integer.MIN_VALUE); } }
	 */

	private LinkedHashMap<KIMEdge, Pair<KIMVertex>> removeVertices(
			List<KIMVertex> list) {
		LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore = new LinkedHashMap<KIMEdge, Pair<KIMVertex>>();
		for (KIMVertex vCur : list) {
			for (KIMEdge edge : graph.getIncidentEdges(vCur)) {
				Pair<KIMVertex> pontas = graph.getEndpoints(edge);
				arcsToRestore.put(edge, pontas);
			}
			graph.removeVertex(vCur);
		}
		return arcsToRestore;
	}

	private void restoreEdges(
			LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore) {
		for (KIMEdge edge : arcsToRestore.keySet()) {
			Pair<KIMVertex> pontas = arcsToRestore.get(edge);
			graph.addEdge(edge, pontas);
		}
	}

	private void restoreVertices(List<KIMVertex> vertices,
			LinkedHashMap<KIMEdge, Pair<KIMVertex>> arcsToRestore) {
		for (KIMVertex v : vertices)
			graph.addVertex(v);
		restoreEdges(arcsToRestore);
	}

	protected Pair<Object> SEP(ShortestPathKIM<KIMVertex, KIMEdge> Ts,
			ShortestPathKIM<KIMVertex, KIMEdge> Tt, KIMVertex s, KIMVertex t,
			int alfa) {
		stopWatchSEP.start();
		Stack<KIMVertex> stack = new Stack<KIMVertex>();
		double distance = new Double(Double.MAX_VALUE);
		Pair<Object> resp = null;
		stack.push(s);
		while (!stack.isEmpty()) {
			KIMVertex u = stack.pop();
			logger.debug("vertice=" + u);
			Set<KIMVertex> sons = Ts.getSons(s, u);
			logger.debug("Sons of " + u + " = " + sons);
			// Set<Vertex> sons = Ts.getSons(s, u);
			if (u.getEpsilon().equals(u.getZeta())) {
				for (KIMEdge outEdge : graph.getOutEdges(u)) {
					KIMVertex v = graph.getOpposite(u, outEdge);
					if (!sons.contains(v)) {
						if (u.getEpsilon() < v.getZeta()) {
							double newDistance = Ts.getDistance(s, u)
									.doubleValue()
									+ nev.transform(outEdge).doubleValue()
									+ Tt.getDistance(t, v).doubleValue();
							logger.debug("Testando o caminho tipo II de " + s
									+ "--->" + u + "->" + v + "--->" + t
									+ " = " + newDistance);
							if (newDistance < distance) {
								distance = newDistance;
								resp = new Pair<Object>(u, outEdge);
								logger.debug("Caminho tipo II criado =" + resp
										+ " -- " + distance);
							}
						}
					}
				}
			} else {
				if (u.getEpsilon().intValue() < u.getZeta().intValue()) {
					double newDistance = Ts.getDistance(s, u).doubleValue()
							+ Tt.getDistance(t, u).doubleValue();

					logger.debug("Testando o caminho tipo I de " + s + "--->"
							+ u + "--->" + t + " = " + newDistance);
					if (newDistance < distance) {
						distance = newDistance;
						resp = new Pair<Object>(u, u);
						logger.debug("Caminho tipo I criado =" + resp + " -- "
								+ distance);
					}
				}
			}
			logger.debug("Adicionando os veŕtices: ");
			for (KIMVertex son : sons)
				if (son.getEpsilon() < alfa) {
					logger.debug(son + ",");
					stack.push(son);
				}
			System.out.println();
		}
		stopWatchSEP.stop();
		return resp;
	}

	public class DeviationArcsMap<E extends KIMEdge> extends
			HashMap<Integer, Set<E>> {

		private static final long serialVersionUID = -7644759197369055186L;

		public DeviationArcsMap(Integer initialCapacity) {
			super(initialCapacity, 1);
		}

		public void put(Integer devNodeIndex, E edge) {
			Set<E> nodes = get(devNodeIndex);
			if (nodes == null) {
				nodes = new HashSet<E>();
				nodes.add(edge);
				put(devNodeIndex, nodes);
			} else
				nodes.add(edge);
		}
	}

}
