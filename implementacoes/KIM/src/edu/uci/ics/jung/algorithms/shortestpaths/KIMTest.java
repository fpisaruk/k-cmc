package edu.uci.ics.jung.algorithms.shortestpaths;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import edu.uci.ics.jung.algorithms.generators.random.ConnectedUndirectedGraphGenerator;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.algorithms.shortestpaths.Path.Factory;
import edu.uci.ics.jung.algorithms.transformation.DirectionTransformer;
import edu.uci.ics.jung.graph.DirectedSparseGraph;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.utils.draw.SimpleGraphDraw;

/**
 * @author Fábio Pisaruk
 * 
 */
public class KIMTest {
	static Logger logger = Logger.getLogger(KIMTest.class);
	BidiMap<KIMVertex, String> mapVertex = new DualHashBidiMap<KIMVertex, String>();
	BidiMap<KIMEdge, Integer> mapEdge = new DualHashBidiMap<KIMEdge, Integer>();
	KIMVertex a = new KIMVertex("a");
	KIMVertex b = new KIMVertex("b");
	KIMVertex c = new KIMVertex("c");
	KIMVertex d = new KIMVertex("d");
	KIMVertex e = new KIMVertex("e");
	KIMEdge ab = new KIMEdge(1);
	KIMEdge ac = new KIMEdge(1);
	KIMEdge ad = new KIMEdge(1);
	KIMEdge bc = new KIMEdge(1);
	KIMEdge be = new KIMEdge(1.5);
	KIMEdge ce = new KIMEdge(3);
	KIMEdge cd = new KIMEdge(2);
	KIMEdge de = new KIMEdge(1);

	private UndirectedGraph<KIMVertex, KIMEdge> getGrafo() {
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		// Insere o caminho a,d,e
		a.setIsOnPath(true);
		d.setIsOnPath(true);
		e.setIsOnPath(true);
		a.setEpsilon(1);
		d.setEpsilon(2);
		e.setEpsilon(3);
		a.setZeta(1);
		d.setZeta(2);
		e.setZeta(3);
		g.addVertex(a);
		g.addVertex(b);
		g.addVertex(c);
		g.addVertex(d);
		g.addVertex(e);
		g.addEdge(ab, a, b);
		g.addEdge(ac, a, c);
		g.addEdge(ad, a, d);
		g.addEdge(bc, b, c);
		g.addEdge(be, b, e);
		g.addEdge(ce, c, e);
		g.addEdge(cd, c, d);
		g.addEdge(de, d, e);

		for (KIMVertex vertex : g.getVertices())
			mapVertex.put(vertex, vertex.getNome());
		for (KIMEdge edge : g.getEdges())
			mapEdge.put(edge, edge.getID());

		return g;
	}

	@Test
	public final void testGetPa() throws IOException {
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		Transformer<KIMEdge, Number> nev = KIMEdge.getDefautTransformer();
		logger.debug(g.getVertices());
		logger.debug(g.getEdges());
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		ArrayList<KIMEdge> list = new ArrayList<KIMEdge>();
		list.add(ad);
		list.add(de);
		Path.Factory pathFactory = new Path.Factory(g, nev);
		Path<KIMVertex, KIMEdge> p1 = pathFactory.newInstance(list, a);
		Path<KIMVertex, KIMEdge> p2 = kim.FSP(a, e, p1);
		Assert.assertFalse(p1.equals(p2));
		p2.setParent(p1);
		Path<KIMVertex, KIMEdge> pa = kim.getPa(a, e, p2);
		Assert.assertFalse(p1.equals(pa) && p2.equals(pa));
		logger.debug("P1 = " + p1);
		logger.debug("P2 = " + p2);
		logger.debug("PA = " + pa);
		logger.debug("</testGetPa>");
	}

	@Test
	public final void testFSP() throws IOException {
		logger.debug("<testFSP>");
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		Transformer<KIMEdge, Number> nev = KIMEdge.getDefautTransformer();
		KIM kim = new KIM(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		// MyGraphDraw.desenha(g, nev);
		ArrayList list = new ArrayList();
		list.add(ad);
		list.add(de);
		Path.Factory pathFactory = new Path.Factory(g, nev);
		Path par = pathFactory.newInstance(list, a);
		Path resp = kim.FSP(a, e, par);
		logger.debug("Novo caminho = " + resp);
		logger.debug("</testFSP>");
	}

	@Test
	public final void testGetPaths() throws IOException {
		logger.debug("<testGetPaths>");
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		g = pnr.load("data/pajNetTest.dat", g);
		Iterator<KIMVertex> i = g.getVertices().iterator();
		BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			atual.setNome(pnr.getVertexLabeller().transform(atual));
			bMap.put(atual, atual.getNome());
			logger.debug(atual + " -> ("
					+ pnr.getVertexLabeller().transform(atual) + ")");
		}
		Iterator<KIMEdge> j = g.getEdges().iterator();
		while (j.hasNext()) {
			KIMEdge atual = j.next();
			logger.debug(atual + " -> " + nev.transform(atual));
		}
		// MyGraphDraw.desenha(g, nev);
		KIMVertex start = bMap.getKey("a");
		KIMVertex target = bMap.getKey("i");
		int k = 100;
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		Path<KIMVertex, KIMEdge> paths[] = kim.getPaths(start, target, k);
		for (int h = 0; h < k; h++)
			logger.debug(paths[h]);
		SimpleGraphDraw.desenha(g, nev, "GRAFO");
		logger.debug("</testGetPaths>");
	}

	@Test
	public final void testGetTreeTs() throws IOException {
		logger.debug("<testGetTreeTs>");
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		Transformer<KIMEdge, Number> nev = KIMEdge.getDefautTransformer();
		KIM kim = new KIM(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		ArrayList<KIMEdge> list = new ArrayList<KIMEdge>();
		list.add(ad);
		list.add(de);
		// list.add(e);
		Path.Factory pathFactory = new Path.Factory(g, nev);
		Path par = pathFactory.newInstance(list, a);
		ShortestPathKIM<KIMVertex, KIMEdge> dspKIM = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TS);
		logger.debug(dspKIM.getIncomingEdgeMap(a));
		Iterator<KIMVertex> i = g.getVertices().iterator();

		while (i.hasNext()) {
			KIMVertex atual = i.next();
			logger.debug(atual + " Epsilon= " + atual.getEpsilon() + " Zeta= "
					+ atual.getZeta() + " -> "
					+ printEdge(dspKIM.getIncomingEdge(a, atual), g));
		}
		logger.debug("</testGetTreeTs>");
	}

	@Test
	public final void testGetTreeTt() throws IOException {
		logger.debug("<testGetTreeTt>");
		UndirectedGraph<KIMVertex, KIMEdge> g = getGrafo();
		Transformer<KIMEdge, Number> nev = KIMEdge.getDefautTransformer();
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		ArrayList<KIMEdge> list = new ArrayList();
		list.add(de);
		list.add(ad);
		Path.Factory pf = new Path.Factory(g, nev);
		Path<KIMVertex, KIMEdge> par = pf.newInstance(list, e);
		ShortestPathKIM<KIMVertex, KIMEdge> dspKIM = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TS);
		System.out
				.println("Arcos incidentes = " + dspKIM.getIncomingEdgeMap(e));
		Iterator<KIMVertex> i = g.getVertices().iterator();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			logger.debug(atual + " Epsilon= " + atual.getEpsilon() + " Zeta= "
					+ atual.getZeta() + " -> "
					+ printEdge(dspKIM.getIncomingEdge(e, atual), g));
		}
		logger.debug("</testGetTreeTt>");
	}

	/*
	 * public final void testGetTrees() throws IOException {
	 * logger.debug("<testGetTrees>"); UndirectedGraph<KIMVertex, KIMEdge> g =
	 * getGrafo(); Transformer<KIMEdge, Number> nev =
	 * KIMEdge.getDefautTransformer(); KIM<KIMVertex, KIMEdge> kim = new
	 * KIM<KIMVertex, KIMEdge>(g, nev,
	 * ShortestPathKIM.DijkstraShortestPathKIMAlgorithm); List<KIMEdge> edges =
	 * new Vector<KIMEdge>(); edges.add(ac); edges.add(ce);
	 * 
	 * Path.PathFactory pathFactory = new Path.PathFactory(g, nev);
	 * Path<KIMVertex, KIMEdge> p = pathFactory.newInstance(edges, a);
	 * ShortestPathKIM<KIMVertex, KIMEdge> Tt = kim.getTreeTt(e, a, p
	 * .getReverse()); ShortestPathKIM<KIMVertex, KIMEdge> Ts = kim.getTreeTs(a,
	 * e, p); Iterator<KIMVertex> i = g.getVertices().iterator();
	 * logger.debug("=============================="); while (i.hasNext()) {
	 * KIMVertex atual = i.next(); logger.debug(atual + " Epsilon= " +
	 * atual.getEpsilon() + " Zeta= " + atual.getZeta());
	 * Assert.assertTrue((Integer) atual.getEpsilon() <= (Integer) atual
	 * .getZeta()); } logger.debug("</testGetTrees>"); }
	 */

	public String printEdge(KIMEdge e, UndirectedGraph<KIMVertex, KIMEdge> g) {
		if (e == null)
			return null;
		KIMVertex incident = (KIMVertex) g.getIncidentVertices(e).iterator()
				.next();
		KIMVertex follow = (KIMVertex) g.getOpposite(incident, e);
		return "(" + incident + "," + follow + ")";
	}

	@Test
	public final void testGetTreesINTERNET() throws IOException {
		logger.debug("<testGetTreeTsINTERNET>");
		PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		Graph<KIMVertex, KIMEdge> dg = new DirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = KIMEdge.getConstantTransformer();
		/*
		 * UndirectedGraph<KIMVertex, KIMEdge> g = new GraphToUndirectGraph()
		 * .transform(pnr .load("data/INTERNET.NET", dg));
		 */
		UndirectedGraph<KIMVertex, KIMEdge> g = DirectionTransformer
				.toUndirected(
						pnr.load("data/INTERNET.NET", dg),
						new org.apache.commons.collections15.Factory<UndirectedGraph<KIMVertex, KIMEdge>>() {
							@Override
							public UndirectedGraph<KIMVertex, KIMEdge> create() {
								return new UndirectedSparseGraph<KIMVertex, KIMEdge>();
							}
						}, KIMEdge.getFactory(), true);

		//new GraphToUndirectGraph().transform(pnr.load("data/INTERNET.NET", dg));
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.BFSShortestPathKIMAlgorithm);
		Iterator<KIMVertex> j = g.getVertices().iterator();
		KIMVertex s = j.next();
		KIMVertex t = j.next();
		Factory pf = new Path.Factory(g, nev);
		Path<KIMVertex, KIMEdge> par = pf.newInstance(new DijkstraShortestPath(
				g, nev).getPath(s, t), s);
		kim.insertPathOnTree(par, 0);
		ShortestPathKIM<KIMVertex, KIMEdge> TS = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.BFSShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TS);
		ShortestPathKIM<KIMVertex, KIMEdge> TT = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.BFSShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TT);
		// logger.debug(TS.getIncomingEdgeMap(s));
		TS.getIncomingEdgeMap(s);
		TT.getIncomingEdgeMap(t);
		Iterator<KIMVertex> i = g.getVertices().iterator();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			logger.info(atual + " Epsilon= " + atual.getEpsilon() + " Zeta= "
					+ atual.getZeta() + " -> "
					+ printEdge(TS.getIncomingEdge(s, atual), g));
			Assert.assertTrue((Integer) atual.getEpsilon() <= (Integer) atual
					.getZeta());
		}
		logger.info(par);
		logger.debug("</testGetTreeTsINTERNET>");
	}

	@Test
	public final void testGetTreesUSPowerGrid() throws IOException {
		logger.debug("<testGetTreesUSPowerGrid>");
		PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<Graph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		g = (UndirectedGraph<KIMVertex, KIMEdge>) pnr.load(
				"data/USpowerGrid.net", g);
		Transformer<KIMEdge, Number> nev = KIMEdge.getDefautTransformer();
		ConnectedUndirectedGraphGenerator.atribuiCustosNasArestas(g, 0, 0);
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		Iterator<KIMVertex> j = g.getVertices().iterator();
		KIMVertex s = j.next();
		KIMVertex t = j.next();
		kim.getPaths(s, t, 100);

		List<KIMEdge> edges = new DijkstraShortestPath<KIMVertex, KIMEdge>(g,
				nev, true).getPath(s, t);
		Path.Factory factory = new Path.Factory(g, nev);
		Path<KIMVertex, KIMEdge> par = factory.newInstance(edges, s);
		int c = 1;
		for (KIMVertex v : par.getVertices()) {
			v.setIsOnPath(true);
			v.setEpsilon(c);
			v.setZeta(c);
			c++;
		}
		ShortestPathKIM<KIMVertex, KIMEdge> TS = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TS);
		ShortestPathKIM<KIMVertex, KIMEdge> TT = new ShortestPathKIM.Factory(
				nev, g, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm)
				.newInstance(ShortestPathKIM.TreeType.TT);
		TS.getIncomingEdgeMap(s);
		TT.getIncomingEdgeMap(t);
		logger.debug("st-caminho:" + par);
		for (KIMVertex atual : g.getVertices()) {
			logger.debug(atual + " Epsilon= " + atual.getEpsilon() + " Zeta= "
					+ atual.getZeta() + " -> "
					+ printEdge(TS.getIncomingEdge(s, atual), g));
			if ((Integer) atual.getEpsilon() > (Integer) atual.getZeta()) {
				logger.error("Ts: " + TS.getPath(s, atual));
				logger.error("Tt: " + TT.getPath(t, atual));
			}
			Assert.assertTrue((Integer) atual.getEpsilon() <= (Integer) atual
					.getZeta());

		}
		logger.debug("</testGetTreesUSPowerGrid>");
	}
}
