/**
 * 
 */
package edu.uci.ics.jung.algorithms.transformation;

import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.UndirectedGraph;

/**
 * 
 * @author Fábio Pisaruk
 * 
 * @param <G>
 * @param <T>
 * @param <V>
 * @param <E>
 */
public class UndirectGraphToTree<G extends UndirectedGraph<V, E>, T extends Tree<V, E>, V, E>
		implements Transformer<G, T> {
	private V root;
	private Map<V, E> edgeMap;

	public UndirectGraphToTree(V root, Map<V, E> edgeMap) {
		this.root = root;
		this.edgeMap = edgeMap;
	}

	@Override
	public T transform(G g) {
		//Iterator<V> it = edgeMap.keySet().iterator();
		LinkedList<V> toVisit = new LinkedList<V>(edgeMap.keySet());
		T tree = (T) new DelegateTree<V, E>();
		tree.addVertex(root);
		while (!toVisit.isEmpty()) {
			V cur = toVisit.pollFirst();
			//System.out.println("(Vertice = " + cur + ")");
			E edge = edgeMap.get(cur);
			//System.out.println("(Arco = " + edge + ")");
			if (edge == null)
				continue;
			V pontaA = g.getEndpoints(edge).getFirst();
			V pontaB = g.getEndpoints(edge).getSecond();
			//System.out.println("PONTA A =  " + g.getEndpoints(edge).getFirst());
			//System.out
			//		.println("PONTA B =  " + g.getEndpoints(edge).getSecond());
			if (tree.containsVertex(pontaA))
				tree.addEdge(edge, pontaA, pontaB);
			else if (tree.containsVertex(pontaB))
				tree.addEdge(edge, pontaB, pontaA);
			else
				toVisit.addLast(cur);
		}
		return tree;
	}
}
