package edu.uci.ics.jung.algorithms.shortestpath;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.shortestpaths.Path;
import edu.uci.ics.jung.graph.UndirectedGraph;

public class BFSKIM<V, E> implements ShortestPathKIM<KIMVertex, KIMEdge> {
	private Map<KIMVertex, KIMEdge> incomingEdgeMap;
	private Map<KIMVertex, Number> distanceMap;
	private UndirectedGraph<KIMVertex, KIMEdge> g;
	private HashMap<KIMVertex, LinkedHashSet<KIMVertex>> sons;
	private ShortestPathKIM.TreeType treeType;
	private KIMVertex source;

	public void reset() {
		incomingEdgeMap = null;
		distanceMap = null;
		source = null;
		sons = null;
	}

	protected BFSKIM(UndirectedGraph<KIMVertex, KIMEdge> g,
			ShortestPathKIM.TreeType treeType) {
		this.g = g;
		this.treeType = treeType;
	}

	public BFSKIM(UndirectedGraph<KIMVertex, KIMEdge> g,
			Transformer<KIMEdge, Number> transformer,
			ShortestPathKIM.TreeType treeType) {
		this.g = g;
		this.treeType = treeType;
	}

	protected BFSKIM(UndirectedGraph<KIMVertex, KIMEdge> g) {
		this.g = g;
	}

	private void bfs(KIMVertex source) {
		LinkedList<KIMVertex> unknown = new LinkedList<KIMVertex>();
		sons = new HashMap<KIMVertex, LinkedHashSet<KIMVertex>>();
		distanceMap = new HashMap<KIMVertex, Number>();
		incomingEdgeMap = new HashMap<KIMVertex, KIMEdge>();
		unknown.add(source);
		distanceMap.put(source, 0);
		while (!unknown.isEmpty()) {
			KIMVertex cur = unknown.pollFirst();
			sons.put(cur, new LinkedHashSet<KIMVertex>());
			Iterator<KIMEdge> i = g.getIncidentEdges(cur).iterator();
			while (i.hasNext()) {
				KIMEdge incomingEdge = i.next();
				KIMVertex neighbor = g.getOpposite(cur, incomingEdge);
				if (!distanceMap.containsKey(neighbor)
						&& !(neighbor.isOnPath() && !cur.isOnPath())) {
					unknown.addLast(neighbor);
					incomingEdgeMap.put(neighbor, incomingEdge);
					distanceMap.put(neighbor,
							(Integer) distanceMap.get(cur) + 1);
					/*
					 * if (!sons.containsKey(cur)) { LinkedHashSet<KIMVertex>
					 * lSons = new LinkedHashSet<KIMVertex>();
					 * lSons.add(neighbor); sons.put(cur, lSons); } else
					 */
					sons.get(cur).add(neighbor);
					if (!neighbor.isOnPath() && treeType != null) {
						if (treeType.equals(ShortestPathKIM.TreeType.TS))
							neighbor.setEpsilon(cur.getEpsilon());
						else if (treeType.equals(ShortestPathKIM.TreeType.TT))
							neighbor.setZeta(cur.getZeta());
					}
				}
			}
		}
		// System.out.println(sons);
	}

	@Override
	public KIMEdge getIncomingEdge(KIMVertex source, KIMVertex target) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		return incomingEdgeMap.get(target);
	}

	@Override
	public List<KIMEdge> getPath(KIMVertex source, KIMVertex target) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		KIMVertex cur = target;
		LinkedList<KIMEdge> path = new LinkedList<KIMEdge>();
		while (cur != source) {
			KIMEdge atual = incomingEdgeMap.get(cur);
			path.addFirst(atual);
			cur = g.getOpposite(cur, atual);
		}
		return (LinkedList<KIMEdge>) path;
	}

	@Override
	public Map<KIMVertex, KIMEdge> getIncomingEdgeMap(KIMVertex source) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		return incomingEdgeMap;
	}

	@Override
	public Number getDistance(KIMVertex source, KIMVertex target) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		return getDistanceMap(source).get(target);
	}

	@Override
	public Map<KIMVertex, Number> getDistanceMap(KIMVertex source) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		return distanceMap;
	}

	@Override
	public LinkedHashSet<KIMVertex> getSons(KIMVertex source, KIMVertex vertex) {
		if (this.source == null || !this.source.equals(source)) {
			this.source = source;
			bfs(source);
		}
		return sons.get(vertex);
	}
}
