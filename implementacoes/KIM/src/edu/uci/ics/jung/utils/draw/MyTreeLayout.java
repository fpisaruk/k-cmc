package edu.uci.ics.jung.utils.draw;

import java.awt.Dimension;

import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.graph.Forest;

public class MyTreeLayout<V, E> extends TreeLayout<V, E> {

	@Override
	public void setSize(Dimension size) {
		this.size = size;
	}

	public MyTreeLayout(Forest<V, E> g, int distx, int disty, Dimension size) {
		super(g, distx, disty); 
		this.size=size;
		super.setGraph(g);
	}
}
