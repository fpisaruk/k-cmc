/**
 * 
 */
package edu.uci.ics.jung.utils;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;

import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpaths.Path;
import edu.uci.ics.jung.graph.DelegateTree;
import edu.uci.ics.jung.graph.Tree;

/**
 * @author Fábio Pisaruk
 * 
 */
public class PathsToTree<P extends Collection<Path<V, E>>, T extends Tree<V, E>, V extends KIMVertex, E extends KIMEdge>
		implements Transformer<P, T> {
	private static Logger logger = Logger
			.getLogger(PathsToTree.class);
	private Transformer<KIMEdge, Number> nev;

	@Override
	public T transform(P paths) {
		System.out.println(paths);
		SortedSet<Path<V, E>> pathsOrdenado = new TreeSet<Path<V, E>>(
				new Comparator<Path<V, E>>() {
					@Override
					public int compare(Path<V, E> o1,
							Path<V, E> o2) {
						if (o1 == null)
							return 1;
						if (o2 == null)
							return -1;
						if (o1.getOrdem() == -1)
							return 1;
						if (o2.getOrdem() == -1)
							return -1;
						return o1.getOrdem()
								- o2.getOrdem();
					}
				});

		pathsOrdenado.addAll(paths);
		Iterator<Path<V, E>> it = pathsOrdenado.iterator();
		// System.out.println(it.next());
		Path<KIMVertex, KIMEdge> p1 = (Path<KIMVertex, KIMEdge>) it
				.next();
		Tree<KIMVertex, KIMEdge> tree = new DelegateTree<KIMVertex, KIMEdge>();
		BidiMap<Node, KIMVertex> nosXvertices = new DualHashBidiMap<Node, KIMVertex>();

		KIMVertex rootNode = new KIMVertex(p1.getStart()
				.getNome());
		tree.addVertex(rootNode);
		nosXvertices.put(new Node(p1, p1.getStart()),
				rootNode);
		KIMVertex lastNode = tree.getRoot();
		for (int i = 1; i < p1.getVertices().size(); i++) {
			KIMVertex node = new KIMVertex(p1.getVertex(i)
					.getNome());
			nosXvertices.put(new Node(p1, p1.getVertex(i)),
					node);
			tree.addEdge(new KIMEdge(nev.transform(p1
					.getEdge(i - 1))), lastNode, node);
			lastNode = node;
		}

		lastNode.setNome(lastNode.getNome() + "1");
		int i = 1;
		while (it.hasNext()) {
			Path atual = it.next();
			if (atual == null)
				break;
			logger.info("Inserindo path=" + atual);
			KIMVertex node = nosXvertices
					.get(new Node(atual.getParent(), atual
							.getDevVertex()));
			KIMVertex curNode = node;
			for (int j = atual.getDevNodeIndex() + 1; j < atual
					.getVertices().size(); j++) {
				KIMVertex newNode = new KIMVertex(atual
						.getVertex(j).getNome());
				tree.addEdge(new KIMEdge(nev
						.transform(atual.getEdge(j - 1))),
						curNode, newNode);
				nosXvertices.put(new Node(atual, atual
						.getVertex(j)), newNode);
				curNode = newNode;
			}
			if (atual.getOrdem() != -1)
				curNode
						.setNome(curNode.getNome()
								+ (i + 1)
								+ "["
								+ (atual.getParent()
										.getOrdem() + 1)
								+ "](" + atual.getOrigem()
								+ ")");
			else
				curNode
						.setNome(curNode.getNome()
								+ atual.getOrigem()
								+ "["
								+ (atual.getParent()
										.getOrdem() + 1)
								+ "]");
			i++;
		}
		logger.info("Arvore de paths=" + tree);
		return (T) tree;
	}

	public PathsToTree(Transformer<KIMEdge, Number> nev) {
		this.nev = nev;
	}

	/* Each node is identified by its path and a vertex in the graph. */
	public class Node {

		@Override
		public int hashCode() {
			return path.hashCode()
					* vertex.getNome().hashCode();
		}

		@Override
		public String toString() {
			return "{" + path + "," + vertex + "}";
		}

		@Override
		public boolean equals(Object other) {
			Node o = (Node) other;
			return this.getPath().equals(o.getPath())
					&& this.getVertex().getNome().equals(
							o.getVertex().getNome());
		}

		private Path path;
		private KIMVertex vertex;

		public Path getPath() {
			return path;
		}

		public void setPath(Path path) {
			this.path = path;
		}

		public KIMVertex getVertex() {
			return vertex;
		}

		public void setVertex(KIMVertex vertex) {
			this.vertex = vertex;
		}

		public Node(Path path, KIMVertex vertex) {
			this.path = path;
			this.vertex = vertex;
		}
	}
}