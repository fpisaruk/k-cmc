/**
 * 
 */
package edu.uci.ics.jung.utils;

/**
 * @author Fábio Pisaruk
 * 
 */
public class Statistics {
	/**
	 * @return the timeTrees
	 */
	public long getTrees() {
		return trees;
	}

	/**
	 * @param timeTrees
	 *            the timeTrees to set
	 */
	public void setTrees(long timeTrees) {
		this.trees = timeTrees;
	}

	/**
	 * @return the sEP
	 */
	public long getSEP() {
		return SEP;
	}

	/**
	 * @param sep
	 *            the sEP to set
	 */
	public void setSEP(long sep) {
		SEP = sep;
	}

	/**
	 * @return the fSP
	 */
	public long getFSP() {
		return FSP;
	}

	/**
	 * @param fsp
	 *            the fSP to set
	 */
	public void setFSP(long fsp) {
		FSP = fsp;
	}

	private long trees;
	private long SEP;
	private long FSP;

	public void addTimeToTree(long time) {
		trees += time;
	}

	public void addTimeToFSP(long time) {
		FSP += time;
	}

	public void addTimeToSEP(long time) {
		SEP += time;
	}

	public static void main(String[] args) {
		StopWatch watch = new StopWatch();
		watch.start("T1");
		t1();
		//watch.stop();
		watch.start("T2");
		t2();
		watch.stop();
		System.out.println(watch.prettyPrint());

	}

	private static void t1() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void t2() {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
