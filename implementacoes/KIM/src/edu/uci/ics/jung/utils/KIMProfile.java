/**
 * 
 */
package edu.uci.ics.jung.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections15.Factory;
import org.apache.commons.collections15.Transformer;
import org.apache.log4j.Logger;

import cern.colt.matrix.doublealgo.Formatter;

import edu.uci.ics.jung.algorithms.generators.random.ConnectedUndirectedGraphGenerator;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.algorithms.shortestpaths.KIM;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.io.PajekNetReader;
import edu.uci.ics.jung.io.PajekNetWriter;

/**
 * @author Fábio Pisaruk
 * 
 */
public class KIMProfile {
	static Logger logger = Logger.getLogger(KIMProfile.class);
	static Factory<KIMVertex> vertexFactory = KIMVertex.getFactory();
	static Factory<KIMEdge> edgeFactory = KIMEdge.getFactory();

	/**
	 * @param m
	 * @param grafo
	 * @return
	 */
	private static void atribuiCustosNosArcos(int m,
			UndirectedGraph<KIMVertex, KIMEdge> grafo) {
		Iterator<KIMEdge> i = grafo.getEdges().iterator();
		Random random = new Random();
		while (i.hasNext()) {
			KIMEdge edge = (KIMEdge) i.next();
			edge.setCost(random.nextInt(m) + 1);
		}
	}

	private static Pair<KIMVertex> getOrigemDestino(List<KIMVertex> vertices) {
		Random random = new Random();
		KIMVertex s, t;
		do {
			s = vertices.get(random.nextInt(vertices.size()));
			t = vertices.get(random.nextInt(vertices.size()));
		} while (s.equals(t));
		return new Pair<KIMVertex>(s, t);
	}

	private static void geraGrafos(float densidade, String nome, int n)
			throws IOException {
		int m = (int) (densidade * n * (n - 1) / 2);
		// for (int k = 100; k <= 1000; k += 100) {
		// logger.fatal("n = " + n + " m = " + m + " k =" + k);
		saveGraph(n, m, nome);
		// }
	}

	public static void main(String[] args) throws IOException {
		float d[] = new float[] { 0.1f, 0.5f, 1.0f };
		StringBuffer resp = new StringBuffer();
		for (int n = 100; n <= 2000; n += 100) {
			for (int i = 0; i < d.length; i++) {
				int m = (int) (d[i] * n * (n - 1) / 2);
				String nomeArquivoDoGrafo = "completo_d_"
						+ Math.round(d[i] * 10) / 10.0;
				geraGrafos(d[i], nomeArquivoDoGrafo, n);
				for (int k = 100; k <= 1000; k += 100) {
					resp.append(getTemposCompletos(nomeArquivoDoGrafo, m, n, k,
							d[i])
							+ "\n");
				}
			}
			saveResp("data_tmp_" + n, resp.toString().substring(0,
					resp.toString().length() - 1));
		}
		saveResp("data_", resp.toString().substring(0,
				resp.toString().length() - 1));

		/*
		 * int d = 1; n = 100; StringBuffer stringBuffer = new StringBuffer();
		 * while (d <= 10) { int m = (int) (d / 10.0f (n (n - 1) / 2));
		 * stringBuffer.append(getTemposPorCaminho("d_" + d / 10.0f, m, n, 1000,
		 * d / 10.0f) + "\n"); d++; } saveResp("custosPorCaminho",
		 * stringBuffer.toString() .substring( 0, stringBuffer.toString()
		 * .length() - 1));
		 */

		/*
		 * int d = 1; int n = 100; StringBuffer stringBuffer = new
		 * StringBuffer(); while (d <= 10) { int m = (int) (d / 10.0f * (n * (n
		 * - 1) / 2)); for (int k = 100; k <= 1000; k += 100) {
		 * stringBuffer.append(getMemoriaTotal("d_" + d / 10.0f, m, n, k, d /
		 * 10.0f) + "\n"); } stringBuffer.append("\n"); d++; }
		 * saveResp("memoria", stringBuffer.toString().substring(0,
		 * stringBuffer.toString().length() - 1));
		 */
	}

	/**
	 * @param string
	 * @param substring
	 * @throws IOException
	 */
	private static void saveResp(String nome, String data) throws IOException {
		FileOutputStream fos = new FileOutputStream("graficos/execucoes/"
				+ nome + ".dat");
		PrintStream printStream = new PrintStream(fos);
		printStream.print(data);
		printStream.close();
	}

	private static void printUsage() {
		System.out.println("Uso: KIMProfile n m k\n"
				+ "\t n Número de vértices\n" + "\t m Número de arcos\n"
				+ "\t n Número de caminhos a serem gerados");
	}

	private int n;

	private int m;

	private int k;

	/**
	 * @param n2
	 * @param m2
	 * @param k2
	 */
	public KIMProfile(int n2, int m2, int k2) {
		n = n2;
		m = m2;
		k = k2;
	}

	/**
	 * Gera e grava um grafo
	 * 
	 * @param n
	 * @param m
	 * @param k
	 * @param fileName
	 * @throws IOException
	 */
	private static void saveGraph(int n, int m, String nome) throws IOException {
		ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge> gerador = new ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge>(
				vertexFactory, edgeFactory, n, m);
		UndirectedGraph<KIMVertex, KIMEdge> grafo = (UndirectedGraph<KIMVertex, KIMEdge>) gerador
				.create();
		atribuiCustosNosArcos(m, grafo);
		PajekNetWriter<KIMVertex, KIMEdge> writer = new PajekNetWriter<KIMVertex, KIMEdge>();
		String filename = "graficos/data/" + nome + "_" + m + "_" + n + ".dat";
		writer.save(grafo, filename, new Transformer<KIMVertex, String>() {
			public String transform(KIMVertex v) {
				return v.getNome();
			}
		}, KIMEdge.getDefautTransformer());
		logger.debug("Salvo grafo: " + filename);
	}

	private static String getTemposCompletos(String nome, int m, int n, int k,
			float densidade) throws IOException {
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				vertexFactory, edgeFactory);
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		String filename = "graficos/data/" + nome + "_" + m + "_" + n + ".dat";
		g = pnr.load(filename, g);
		for (KIMVertex v : g.getVertices())
			v.setNome(pnr.getVertexLabeller().transform(v));
		List<KIMVertex> vertices = new ArrayList<KIMVertex>(g.getVertices());
		// MyGraphDraw.desenha(grafo, KIMEdge.getDefautTransformer());
		int timeTress = 0, timeSEP = 0, timeKIM = 0;
		for (int i = 1; i <= 5; i++) {
			Pair<KIMVertex> pontas = getOrigemDestino(vertices);
			logger.fatal("Rodando arquivo:" + filename + " rodada " + i + " n="
					+ n + " m=" + m + " k=" + k);
			try {
				KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g,
						nev, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
				kim.getPaths(pontas.getFirst(), pontas.getSecond(), k);
				timeTress += kim.getStopWatchTrees().getTotalTimeMillis();
				timeKIM += kim.getStopWatchKIM().getTotalTimeMillis();
				timeSEP += kim.getStopWatchSEP().getTotalTimeMillis();
			} catch (NullPointerException e) {
				System.err.println("DEU ERRO pontas="
						+ pnr.getVertexLabeller().transform(pontas.getFirst())
						+ ","
						+ pnr.getVertexLabeller().transform(pontas.getSecond())
						+ " nome=" + nome + " n=" + n + " m=" + m + " k=" + k
						+ "\n");
				System.err.println("Rodando arquivo:" + filename + " rodada "
						+ i + " n=" + n + " m=" + m + " k=" + k);
				throw e;
				// i--;
			}
		}
		String resp = densidade + ";" + n + ";" + m + ";" + k + ";"
				+ (int) timeTress / 5 + ";" + (int) timeSEP / 5 + ";"
				+ (int) timeKIM / 5;
		return resp;
	}

	private static String getTemposPorCaminho(String nome, int m, int n, int k,
			float d) throws IOException {
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				vertexFactory, edgeFactory);
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		String filename = "graficos/data/" + nome + "_" + m + "_" + n + ".dat";
		g = pnr.load(filename, g);

		List<KIMVertex> vertices = new ArrayList<KIMVertex>(g.getVertices());
		// MyGraphDraw.desenha(grafo, KIMEdge.getDefautTransformer());
		StringBuffer resp = new StringBuffer(k * 3);
		int tempos[] = new int[k];
		for (int j = 0; j < k; j++)
			tempos[j] = 0;
		for (int i = 1; i <= 5; i++) {
			Pair<KIMVertex> pontas = getOrigemDestino(vertices);
			logger.fatal("Rodando arquivo:" + filename + " rodada " + i + " n="
					+ n + " m=" + m + " k=" + k);
			try {
				KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g,
						nev, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
				kim.getPaths(pontas.getFirst(), pontas.getSecond(), k);
				for (int j = 0; j < k; j++) {
					tempos[j] += kim.getStopWatchPerPath().getTaskInfo()[j]
							.getTimeMillis();
				}
			} catch (NullPointerException e) {
				System.err.println("DEU ERRO pontas=" + pontas + " nome="
						+ nome + " n=" + n + " m=" + m + " k=" + k + "\n");
				i--;
			}
		}
		for (int j = 0; j < k; j++) {
			resp.append(n + ";" + m + ";" + tempos[j] / 5 + ";" + (j + 1) + ";"
					+ d + "\n");
		}

		return resp.toString();
	}

	private static String getMemoriaTotal(String nome, int m, int n, int k,
			float d) throws IOException {
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				vertexFactory, edgeFactory);
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		String filename = "graficos/data/" + nome + "_" + m + "_" + n + ".dat";
		g = pnr.load(filename, g);

		List<KIMVertex> vertices = new ArrayList<KIMVertex>(g.getVertices());
		// MyGraphDraw.desenha(grafo, KIMEdge.getDefautTransformer());
		int memoria = 0;
		int avgMem = 0;
		for (int i = 1; i <= 5; i++) {
			Pair<KIMVertex> pontas = getOrigemDestino(vertices);
			logger.fatal("Rodando arquivo:" + filename + " rodada " + i + " n="
					+ n + " m=" + m + " k=" + k);
			try {
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				Runtime.getRuntime().gc();
				KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g,
						nev, ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
				long initMem = Runtime.getRuntime().totalMemory()
						- Runtime.getRuntime().freeMemory();
				kim.getPaths(pontas.getFirst(), pontas.getSecond(), k);
				long finalMem = Runtime.getRuntime().totalMemory()
						- Runtime.getRuntime().freeMemory();
				long memUsage = finalMem - initMem;
				avgMem += kim.getAvgMem();
				memoria += memUsage;
			} catch (NullPointerException e) {
				System.err.println("DEU ERRO pontas=" + pontas + " nome="
						+ nome + " n=" + n + " m=" + m + " k=" + k + "\n");
				i--;
			}
		}
		String resp = n + ";" + m + ";" + k + ";" + (int) memoria / 5 + ";"
				+ avgMem / 5 + ";" + d;
		return resp;
	}

	private String rodar() {
		UndirectedGraph<KIMVertex, KIMEdge> grafo = null;
		Pair<KIMVertex> pontas = null;
		try {
			String resp = "";
			ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge> gerador = new ConnectedUndirectedGraphGenerator<KIMVertex, KIMEdge>(
					vertexFactory, edgeFactory, n, m);
			grafo = (UndirectedGraph<KIMVertex, KIMEdge>) gerador.create();
			atribuiCustosNosArcos(m, grafo);
			KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(grafo,
					KIMEdge.getDefautTransformer(),
					ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
			List<KIMVertex> vertices = new ArrayList<KIMVertex>(grafo
					.getVertices());
			// MyGraphDraw.desenha(grafo, KIMEdge.getDefautTransformer());
			pontas = getOrigemDestino(vertices);
			kim.getPaths(pontas.getFirst(), pontas.getSecond(), k);
			resp.concat(n + ";" + m + ";" + k + ";"
					+ kim.getStopWatchTrees().getTotalTimeMillis() + ";"
					+ kim.getStopWatchSEP().getTotalTimeMillis() + ";"
					+ kim.getStopWatchKIM().getTotalTimeMillis());
			return resp;
		} catch (Exception e) {
			PajekNetWriter<KIMVertex, KIMEdge> writer = new PajekNetWriter<KIMVertex, KIMEdge>();
			try {
				writer.save(grafo, "/graficos/completo"
						+ ((int) Math.random() * 100),
						new Transformer<KIMVertex, String>() {
							public String transform(KIMVertex v) {
								return v.getNome();
							}
						}, KIMEdge.getDefautTransformer());
				System.out.println(pontas);
				e.printStackTrace();
				throw new RuntimeException();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return "";
		}
	}
}
