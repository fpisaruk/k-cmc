/**
 * 
 */
package edu.uci.ics.jung.utils;

import java.io.IOException;
import java.util.Iterator;

import org.apache.commons.collections15.BidiMap;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.collections15.bidimap.DualHashBidiMap;

import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.algorithms.shortestpaths.KIM;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.io.PajekNetReader;

/**
 * @author Fábio Pisaruk
 * 
 */
public class KIMSimulacao {
	public static void main(String[] args) throws IOException {
		PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge> pnr = new PajekNetReader<UndirectedGraph<KIMVertex, KIMEdge>, KIMVertex, KIMEdge>(
				KIMVertex.getFactory(), KIMEdge.getFactory());
		UndirectedGraph<KIMVertex, KIMEdge> g = new UndirectedSparseGraph<KIMVertex, KIMEdge>();
		Transformer<KIMEdge, Number> nev = pnr.getEdgeWeightTransformer();
		String filename = "data/pajNetSimulacao.dat";
		g = pnr.load(filename, g);
		Iterator<KIMVertex> i = g.getVertices().iterator();
		BidiMap<KIMVertex, String> bMap = new DualHashBidiMap<KIMVertex, String>();
		while (i.hasNext()) {
			KIMVertex atual = i.next();
			atual.setNome(pnr.getVertexLabeller().transform(atual));
			bMap.put(atual, atual.getNome());
		}
		KIM<KIMVertex, KIMEdge> kim = new KIM<KIMVertex, KIMEdge>(g, nev,
				ShortestPathKIM.DijkstraShortestPathKIMAlgorithm);
		kim.getPaths(bMap.getKey("a"), bMap.getKey("d"), 3);
	}
}
