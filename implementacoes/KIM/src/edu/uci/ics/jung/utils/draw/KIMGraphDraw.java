/**
 * 
 */
package edu.uci.ics.jung.utils.draw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.Collection;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.algorithms.shortestpath.ShortestPathKIM;
import edu.uci.ics.jung.algorithms.shortestpaths.Path;
import edu.uci.ics.jung.algorithms.transformation.UndirectGraphToTree;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.UndirectedGraph;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.utils.PathsToTree;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.GraphZoomScrollPane;
import edu.uci.ics.jung.visualization.VisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.BasicVertexLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class KIMGraphDraw extends JFrame implements Runnable {
	private static final long serialVersionUID = 5374335358610838068L;
	private Graph<KIMVertex, KIMEdge> grafo;
	private Dimension preferredSize = new Dimension(600, 200);

	private final VisualizationViewer<KIMVertex, KIMEdge> vvGraph;
	private final VisualizationViewer<KIMVertex, KIMEdge> vvTS;
	private final VisualizationViewer<KIMVertex, KIMEdge> vvTT;
	private final VisualizationViewer<KIMVertex, KIMEdge> vvPaths;
	private KIMEdgeToString nev;

	private class KIMEdgeToString implements Transformer<KIMEdge, String> {

		private Transformer<KIMEdge, Number> wrapped;

		@Override
		public String transform(KIMEdge arg0) {
			Object resp = wrapped.transform(arg0);
			if (resp != null)
				return resp.toString();
			else
				return null;
		}

		public KIMEdgeToString(Transformer<KIMEdge, Number> transf) {
			wrapped = transf;
		}
	}

	private DefaultModalGraphMouse<KIMVertex, KIMEdge> gm;

	public KIMGraphDraw(UndirectedGraph<KIMVertex, KIMEdge> graph,
			Tree<KIMVertex, KIMEdge> Ts, Tree<KIMVertex, KIMEdge> Tt,
			Transformer<KIMEdge, Number> nev) {
		this(graph, Ts, Tt, nev, null);

	}

	public KIMGraphDraw(UndirectedGraph<KIMVertex, KIMEdge> graph,
			Tree<KIMVertex, KIMEdge> Ts, Tree<KIMVertex, KIMEdge> Tt,
			Transformer<KIMEdge, Number> nev, Tree<KIMVertex, KIMEdge> pathsTree) {
		this.grafo = graph;
		this.nev = new KIMEdgeToString(nev);
		FRLayout<KIMVertex, KIMEdge> graphLayout = new FRLayout<KIMVertex, KIMEdge>(
				graph);
		graphLayout.setSize(new Dimension(650, 350));
		MyTreeLayout<KIMVertex, KIMEdge> tsLayout = new MyTreeLayout<KIMVertex, KIMEdge>(
				Ts, 75, 50, new Dimension(200, 300));
		MyTreeLayout<KIMVertex, KIMEdge> ttLayout = new MyTreeLayout<KIMVertex, KIMEdge>(
				Tt, 75, 50, new Dimension(200, 300));

		vvGraph = new VisualizationViewer<KIMVertex, KIMEdge>(
				new DefaultVisualizationModel<KIMVertex, KIMEdge>(graphLayout));

		vvTS = new VisualizationViewer<KIMVertex, KIMEdge>(
				new DefaultVisualizationModel<KIMVertex, KIMEdge>(tsLayout));
		vvTT = new VisualizationViewer<KIMVertex, KIMEdge>(
				new DefaultVisualizationModel<KIMVertex, KIMEdge>(ttLayout));
		if (pathsTree != null) {
			TreeLayout<KIMVertex, KIMEdge> pathTreeLayout = new TreeLayout<KIMVertex, KIMEdge>(
					pathsTree, 100, 50);
			vvPaths = new VisualizationViewer<KIMVertex, KIMEdge>(
					new DefaultVisualizationModel<KIMVertex, KIMEdge>(
							pathTreeLayout));
			vvPaths.getRenderContext().setLabelOffset(30);
		} else
			vvPaths = null;
		vvGraph.getRenderContext().setLabelOffset(30);
		vvTS.getRenderContext().setLabelOffset(30);
		vvTT.getRenderContext().setLabelOffset(30);

	}

	@Override
	public void run() {
		addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
			}

			@Override
			public void windowClosed(WindowEvent e) {
				System.out.println("FECHADA");

			}

			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("FECHANDO");
				synchronized (grafo) {
					grafo.notify();
				}
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				System.out.println("DESATIVADA");

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}
		});
		setBackground(Color.WHITE);
		vvGraph.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex v) {
						return v.getNome();
					}
				});
		vvGraph.getRenderContext().setEdgeLabelTransformer(nev);
		vvGraph.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
		vvGraph.getRenderContext().setVertexFillPaintTransformer(
				new Transformer<KIMVertex, Paint>() {
					@Override
					public Paint transform(KIMVertex v) {
						if (v.isOnPath())
							return Color.RED;
						else
							return Color.BLUE;
					}
				});
		vvGraph.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());
		vvTS.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex arg0) {
						return arg0.getNome() + "(" + arg0.getEpsilon() + ")";
					}
				});
		vvTS.getRenderContext().setVertexFillPaintTransformer(
				new Transformer<KIMVertex, Paint>() {
					@Override
					public Paint transform(KIMVertex arg0) {
						if (arg0.isOnPath())
							return Color.RED;
						else
							return Color.BLUE;
					}
				});
		vvTS
				.getRenderContext()
				.setEdgeLabelClosenessTransformer(
						new Transformer<Context<Graph<KIMVertex, KIMEdge>, KIMEdge>, Number>() {
							@Override
							public Number transform(
									Context<Graph<KIMVertex, KIMEdge>, KIMEdge> arg0) {
								return 0.5;
							}

						});
		vvTS.getRenderContext().setEdgeShapeTransformer(
				new EdgeShape.Line<KIMVertex, KIMEdge>());
		vvTS.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());
		vvTS.getRenderContext().setEdgeLabelTransformer(nev);
		vvTS.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
		vvTS.getRenderContext().getEdgeLabelRenderer()
				.setRotateEdgeLabels(true);

		vvTT.getRenderContext().setEdgeShapeTransformer(
				new EdgeShape.Line<KIMVertex, KIMEdge>());
		vvTT.getRenderContext().setVertexFillPaintTransformer(
				new Transformer<KIMVertex, Paint>() {
					@Override
					public Paint transform(KIMVertex arg0) {
						if (arg0.isOnPath())
							return Color.RED;
						else
							return Color.BLUE;
					}
				});
		vvTT.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex arg0) {
						return arg0.getNome() + "(" + arg0.getZeta() + ")";
					}
				});
		vvTT.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());
		vvTT.getRenderContext().setEdgeLabelTransformer(nev);
		vvTT.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
		vvTT.getRenderContext().getEdgeLabelRenderer()
				.setRotateEdgeLabels(true);

		if (vvPaths != null) {
			vvPaths.getRenderContext().setVertexLabelTransformer(
					new Transformer<KIMVertex, String>() {
						@Override
						public String transform(KIMVertex arg0) {
							return arg0.getNome();
						}
					});
			vvPaths.getRenderContext().setVertexFillPaintTransformer(
					new Transformer<KIMVertex, Paint>() {
						@Override
						public Paint transform(KIMVertex v) {
							if (vvPaths.getGraphLayout().getGraph().getInEdges(
									v).size() == 0
									|| vvPaths.getGraphLayout().getGraph()
											.getOutEdges(v).size() == 0)
								return Color.RED;
							else
								return Color.BLUE;
						}
					});

			vvPaths
					.getRenderContext()
					.setEdgeLabelClosenessTransformer(
							new Transformer<Context<Graph<KIMVertex, KIMEdge>, KIMEdge>, Number>() {

								@Override
								public Number transform(
										Context<Graph<KIMVertex, KIMEdge>, KIMEdge> arg0) {
									return 0.5;
								}

							});
			vvPaths.getRenderContext().setEdgeShapeTransformer(
					new EdgeShape.Line<KIMVertex, KIMEdge>());
			vvPaths.getRenderContext().setEdgeLabelTransformer(
					new KIMEdgeToString(KIMEdge.getDefautTransformer()));
			vvPaths
					.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
			vvPaths.getRenderContext().getEdgeLabelRenderer()
					.setRotateEdgeLabels(true);
		}
		vvGraph.getRenderer().setVertexLabelRenderer(
				new BasicVertexLabelRenderer(Position.AUTO));
		vvTS.getRenderer().setVertexLabelRenderer(
				new BasicVertexLabelRenderer(Position.AUTO));
		vvTT.getRenderer().setVertexLabelRenderer(
				new BasicVertexLabelRenderer(Position.AUTO));
		if (vvPaths != null)
			vvPaths.getRenderer().setVertexLabelRenderer(
					new BasicVertexLabelRenderer(Position.AUTO));

		/*
		 * vvGraph.addPostRenderPaintable(new BannerLabel(vvGraph, "Grafo"));
		 * vvTS.addPostRenderPaintable(new BannerLabel(vvTS, "Árvore TS"));
		 * vvTT.addPostRenderPaintable(new BannerLabel(vvTT, "Árvore TT"));
		 */

		gm = new DefaultModalGraphMouse<KIMVertex, KIMEdge>();
		gm.setMode(ModalGraphMouse.Mode.PICKING);
		vvGraph.setGraphMouse(gm);
		vvTS.setGraphMouse(gm);
		vvTT.setGraphMouse(gm);
		if (vvPaths != null)
			vvPaths.setGraphMouse(gm);

		JPanel mainPanel = new JPanel(new GridLayout(1, 2));
		JPanel leftPanel = new JPanel(new GridLayout(2, 1));
		JPanel arvores = new JPanel(new GridLayout(1, 3));

		final JPanel graphPanel = new JPanel(new BorderLayout());
		final JPanel arvoreTsPanel = new JPanel(new BorderLayout());
		final JPanel arvoreTtPanel = new JPanel(new BorderLayout());
		final JPanel pathsTreePanel = new JPanel(new BorderLayout());

		GraphZoomScrollPane graphZoomScrollPane1 = new GraphZoomScrollPane(
				vvGraph);

		graphPanel.add(graphZoomScrollPane1);
		arvoreTsPanel.add(new GraphZoomScrollPane(vvTS));
		arvoreTtPanel.add(new GraphZoomScrollPane(vvTT));
		if (vvPaths != null)
			pathsTreePanel.add(new GraphZoomScrollPane(vvPaths));

		arvores.add(arvoreTsPanel);
		arvores.add(arvoreTtPanel);
		arvores.add(pathsTreePanel);

		leftPanel.add(graphPanel);
		leftPanel.add(arvores);

		mainPanel.add(leftPanel);
		mainPanel.add(pathsTreePanel);

		arvores.setBackground(Color.WHITE);
		mainPanel.setBackground(Color.WHITE);
		graphPanel.setBackground(Color.WHITE);
		arvoreTsPanel.setBackground(Color.WHITE);
		arvoreTtPanel.setBackground(Color.WHITE);
		pathsTreePanel.setBackground(Color.WHITE);
		vvGraph.setBackground(Color.WHITE);
		vvTS.setBackground(Color.WHITE);
		vvTT.setBackground(Color.WHITE);
		if (vvPaths != null)
			vvPaths.setBackground(Color.WHITE);
		pathsTreePanel.setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		add(mainPanel, BorderLayout.CENTER);
		JPanel flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		flow.add(setUpControls(vvGraph));
		graphPanel.add(flow, BorderLayout.SOUTH);
		flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		flow.add(setUpControls(vvTS));
		arvoreTsPanel.add(flow, BorderLayout.SOUTH);
		flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		flow.add(setUpControls(vvTT));
		arvoreTtPanel.add(flow, BorderLayout.SOUTH);
		flow = new JPanel(new FlowLayout(FlowLayout.CENTER));
		if (vvPaths != null)
			flow.add(setUpControls(vvPaths));
		pathsTreePanel.add(flow, BorderLayout.SOUTH);
		pack();
		// this.setExtendedState(MAXIMIZED_BOTH);
		setVisible(true);
		/*
		 * vvGraph.setSize(graphPanel.getSize());
		 * graphZoomScrollPane1.setSize(vvGraph.getSize());
		 * graphZoomScrollPane1.invalidate();
		 * vvGraph.setPreferredSize(vvGraph.getSize());
		 * vvGraph.getGraphLayout().setSize(vvGraph.getSize());
		 * vvGraph.repaint();
		 */
		// vvGraph.getGraphLayout().setSize(vvGraph.getSize());
		graphZoomScrollPane1.getVerticalScrollBar().setSize(vvGraph.getSize());
		// graphPanel.repaint();
		System.out.println(vvTS.getGraphLayout().getSize());

		System.out.println("CRIADA");

	}

	public class MyEdgePaintFunction implements Transformer<KIMEdge, Paint> {
		public Paint transform(KIMEdge e) {
			if (grafo.getEndpoints(e).getFirst().isOnPath()
					&& grafo.getEndpoints(e).getSecond().isOnPath())
				return Color.RED;
			return Color.BLACK;
		}
	}

	public class EdgeCostLabeller implements Transformer<KIMEdge, String> {
		@Override
		public String transform(KIMEdge arg0) {
			return nev.transform(arg0).toString();
		}
	}

	private JPanel setUpControls(
			final VisualizationViewer<KIMVertex, KIMEdge> vv) {
		final JPanel jp = new JPanel();
		jp.setBackground(Color.WHITE);
		jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
		JButton salvar = new JButton("Salvar como eps");
		salvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				if (fc.showSaveDialog(jp) == JFileChooser.APPROVE_OPTION) {
					saveGraphAsEPS(fc.getSelectedFile(), vv);
				}
			}
		});
		jp.add(salvar);
		return jp;
	}

	public static final void desenha(UndirectedGraph g1, Tree g2, Tree g3,
			Transformer nev) {
		Thread desenho = new Thread(new KIMGraphDraw(g1, g2, g3, nev),
				"DESENHO");
		desenho.start();
		synchronized (g1) {
			try {
				g1.wait();
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public static final void desenha(UndirectedGraph g1, Tree g2, Tree g3,
			Tree paths, Transformer nev) {
		Thread desenho = new Thread(new KIMGraphDraw(g1, g2, g3, nev),
				"DESENHO");
		desenho.start();
		synchronized (g1) {
			try {
				g1.wait();
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public KIMGraphDraw(UndirectedGraph<KIMVertex, KIMEdge> g1,
			ShortestPathKIM<KIMVertex, KIMEdge> g2, KIMVertex rootA,
			ShortestPathKIM<KIMVertex, KIMEdge> g3, KIMVertex rootB,
			Transformer<KIMEdge, Number> nev) {
		this(g1, new UndirectGraphToTree(rootA, g2.getIncomingEdgeMap(rootA))
				.transform(g1), new UndirectGraphToTree(rootB, g3
				.getIncomingEdgeMap(rootB)).transform(g1), nev);

	}

	public KIMGraphDraw(UndirectedGraph<KIMVertex, KIMEdge> g1,
			ShortestPathKIM<KIMVertex, KIMEdge> g2, KIMVertex rootA,
			ShortestPathKIM<KIMVertex, KIMEdge> g3, KIMVertex rootB,
			Transformer<KIMEdge, Number> nev, Path[] paths) {
		this(g1, new UndirectGraphToTree(rootA, g2.getIncomingEdgeMap(rootA))
				.transform(g1), new UndirectGraphToTree(rootB, g3
				.getIncomingEdgeMap(rootB)).transform(g1), nev,
				new PathsToTree(nev).transform(Arrays.asList(paths)));

	}

	public KIMGraphDraw(UndirectedGraph<KIMVertex, KIMEdge> g1,
			ShortestPathKIM<KIMVertex, KIMEdge> g2, KIMVertex rootA,
			ShortestPathKIM<KIMVertex, KIMEdge> g3, KIMVertex rootB,
			Transformer<KIMEdge, Number> nev, Collection paths) {
		this(g1, new UndirectGraphToTree(rootA, g2.getIncomingEdgeMap(rootA))
				.transform(g1), new UndirectGraphToTree(rootB, g3
				.getIncomingEdgeMap(rootB)).transform(g1), nev,
				new PathsToTree(nev).transform(paths));

	}

	public static final void desenha(UndirectedGraph<KIMVertex, KIMEdge> g1,
			ShortestPathKIM<KIMVertex, KIMEdge> g2, KIMVertex rootA,
			ShortestPathKIM<KIMVertex, KIMEdge> g3, KIMVertex rootB,
			Transformer nev) {
		Tree<KIMVertex, KIMEdge> TS = new UndirectGraphToTree(rootA, g2
				.getIncomingEdgeMap(rootA)).transform(g1);
		Tree<KIMVertex, KIMEdge> TT = new UndirectGraphToTree(rootB, g3
				.getIncomingEdgeMap(rootB)).transform(g1);

		Thread desenho = new Thread(new KIMGraphDraw(g1, TS, TT, nev),
				"DESENHO");
		desenho.start();
		synchronized (g1) {
			try {
				g1.wait();
				System.out.println("SAIU");
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public static final void desenha(UndirectedGraph<KIMVertex, KIMEdge> g1,
			ShortestPathKIM<KIMVertex, KIMEdge> g2, KIMVertex rootA,
			ShortestPathKIM<KIMVertex, KIMEdge> g3, KIMVertex rootB,
			Transformer nev, Path<KIMVertex, KIMEdge>[] paths) {
		Tree<KIMVertex, KIMEdge> TS = new UndirectGraphToTree(rootA, g2
				.getIncomingEdgeMap(rootA)).transform(g1);
		Tree<KIMVertex, KIMEdge> TT = new UndirectGraphToTree(rootB, g3
				.getIncomingEdgeMap(rootB)).transform(g1);
		Tree<KIMVertex, KIMEdge> pathsTree = new PathsToTree(nev)
				.transform(Arrays.asList(paths));
		Thread desenho = new Thread(
				new KIMGraphDraw(g1, TS, TT, nev, pathsTree), "DESENHO");
		desenho.start();
		synchronized (g1) {
			try {
				g1.wait();
				System.out.println("SAIU");
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public void saveGraphAsEPS(File file,
			VisualizationViewer<KIMVertex, KIMEdge> vv) {
		// BufferedImage myImage = new BufferedImage(vv.getWidth(),
		// vv.getHeight(), BufferedImage.TYPE_INT_RGB);
		// Graphics2D g2 = myImage.createGraphics();
		// Color bg = vv.getBackground();
		// g2.setColor(bg);
		// g2.fillRect(0, 0, vv.getWidth(), vv.getHeight());
		vv.setDoubleBuffered(true);
		// vv.paintAll(g2);
		try {
			// ImageIO.write(myImage, "gif", new File(filename));
			FileOutputStream fos = new FileOutputStream(file);
			EpsGraphics epsGraphics = new EpsGraphics("TESTE", fos, 0, 0, vv
					.getWidth(), vv.getHeight(), ColorMode.COLOR_RGB);
			vv.paintAll(epsGraphics);
			fos.flush();
			epsGraphics.close();
		} catch (Throwable e) {
			System.out.println(e);
		}
	}

	class BannerLabel implements VisualizationViewer.Paintable {
		int x;
		int y;
		Font font;
		FontMetrics metrics;
		int swidth;
		int sheight;
		String str;
		VisualizationViewer vv;

		public BannerLabel(VisualizationViewer vv, String label) {
			this.vv = vv;
			this.str = label;
		}

		public void paint(Graphics g) {
			Dimension d = vv.getSize();
			if (font == null) {
				font = new Font(g.getFont().getName(), Font.BOLD, 20);
				metrics = g.getFontMetrics(font);
				swidth = metrics.stringWidth(str);
				sheight = metrics.getMaxAscent() + metrics.getMaxDescent();
				x = (3 * d.width / 2 - swidth) / 2;
				y = d.height - sheight;
			}
			g.setFont(font);
			Color oldColor = g.getColor();
			g.setColor(Color.gray);
			g.drawString(str, x, y);
			g.setColor(oldColor);
		}

		public boolean useTransform() {
			return false;
		}
	}
}
