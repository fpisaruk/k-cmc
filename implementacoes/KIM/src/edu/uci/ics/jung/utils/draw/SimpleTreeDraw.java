/**
 * 
 */
package edu.uci.ics.jung.utils.draw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.layout.TreeLayout;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.graph.util.Context;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.BasicVertexLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class SimpleTreeDraw extends JFrame implements Runnable {
	private static final long serialVersionUID = 5374335358610838068L;
	Tree<KIMVertex, KIMEdge> tree;
	private VisualizationViewer<KIMVertex, KIMEdge> vvPaths;
	private MyTransform nev;
	private boolean rotated;

	private class MyTransform implements Transformer<KIMEdge, String> {

		private Transformer<KIMEdge, Number> wrapped;

		@Override
		public String transform(KIMEdge arg0) {
			if (wrapped == null)
				return null;
			Object resp = wrapped.transform(arg0);
			if (resp != null)
				return resp.toString();
			else
				return null;
		}

		public MyTransform(Transformer<KIMEdge, Number> transf) {
			wrapped = transf;
		}
	}

	private Layout<KIMVertex, KIMEdge> layout;
	private DefaultModalGraphMouse gm;

	public SimpleTreeDraw(Tree tree, Transformer<KIMEdge, Number> nev,
			boolean rotated) {
		this(tree, nev, new TreeLayout<KIMVertex, KIMEdge>(tree, 50, 100),
				rotated);
	}

	public SimpleTreeDraw(Tree tree, Transformer<KIMEdge, Number> nev,
			Layout layout, boolean rotated) {
		this.tree = tree;
		this.nev = new MyTransform(nev);
		this.layout = layout;
		this.rotated = rotated;
		vvPaths = new VisualizationViewer(new DefaultVisualizationModel(layout));
		init();
	}

	@Override
	public void run() {
		addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent e) {
				System.out.println("FECHADA");

			}

			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("FECHANDO");
				synchronized (tree) {
					tree.notify();
				}
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				System.out.println("DESATIVADA");

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
			}
		});
		pack();
		setVisible(true);
		System.out.println("CRIADA");
	}

	private void setLtoR(VisualizationViewer vv) {
		Layout<String, Integer> layout = vv.getModel().getGraphLayout();
		Dimension d = vv.getSize(); // layout.getSize();
		Point2D center = new Point2D.Double(d.width / 2, d.height / 2);
		vv.getRenderContext().getMultiLayerTransformer().getTransformer(
				Layer.LAYOUT).rotate(-Math.PI / 2, center);
	}

	private void init() {
		vvPaths.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex arg0) {
						return arg0.getNome();
					}
				});
		vvPaths.getRenderContext().setVertexFillPaintTransformer(
				new Transformer<KIMVertex, Paint>() {
					@Override
					public Paint transform(KIMVertex v) {
						if (vvPaths.getGraphLayout().getGraph().getInEdges(v)
								.size() == 0
								|| vvPaths.getGraphLayout().getGraph()
										.getOutEdges(v).size() == 0)
							return Color.RED;
						else
							return Color.BLUE;
					}
				});

		vvPaths
				.getRenderContext()
				.setEdgeLabelClosenessTransformer(
						new Transformer<Context<Graph<KIMVertex, KIMEdge>, KIMEdge>, Number>() {

							@Override
							public Number transform(
									Context<Graph<KIMVertex, KIMEdge>, KIMEdge> arg0) {
								return 0.5;
							}

						});
		vvPaths.getRenderContext().setEdgeShapeTransformer(
				new EdgeShape.Line<KIMVertex, KIMEdge>());
		vvPaths.getRenderContext().setEdgeLabelTransformer(
				new KIMEdgeToString(KIMEdge.getDefautTransformer()));
		vvPaths.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
		vvPaths.getRenderContext().getEdgeLabelRenderer().setRotateEdgeLabels(
				true);
		vvPaths.getRenderContext().setLabelOffset(30);
		vvPaths.getRenderer().setVertexLabelRenderer(
				new BasicVertexLabelRenderer(Position.AUTO));
		vvPaths.setBackground(Color.WHITE);
		gm = new DefaultModalGraphMouse();
		gm.setMode(ModalGraphMouse.Mode.PICKING);
		vvPaths.setGraphMouse(gm);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		add(vvPaths, BorderLayout.CENTER);
		add(setUpControls(), BorderLayout.SOUTH);
		pack();
		if (rotated)
			setLtoR(vvPaths);
	}

	private class KIMEdgeToString implements Transformer<KIMEdge, String> {

		private Transformer<KIMEdge, Number> wrapped;

		@Override
		public String transform(KIMEdge arg0) {
			Object resp = wrapped.transform(arg0);
			if (resp != null)
				return resp.toString();
			else
				return null;
		}

		public KIMEdgeToString(Transformer<KIMEdge, Number> transf) {
			wrapped = transf;
		}
	}

	public class MyEdgePaintFunction implements Transformer<KIMEdge, Paint> {
		public Paint transform(KIMEdge e) {
			if (vvPaths.getGraphLayout().getGraph().getEndpoints(e).getFirst()
					.isOnPath()
					&& vvPaths.getGraphLayout().getGraph().getEndpoints(e)
							.getSecond().isOnPath())
				return Color.RED;
			return Color.BLACK;
		}
	}

	public class EdgeCostLabeller implements Transformer<KIMEdge, String> {
		@Override
		public String transform(KIMEdge arg0) {
			return nev.transform(arg0).toString();
		}
	}

	private JPanel setUpControls() {
		final JPanel jp = new JPanel();
		jp.setBackground(Color.WHITE);
		jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
		JButton salvar = new JButton("Salvar como eps");
		salvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"Encapsulated PostScript", "eps");
				fc.setFileFilter(filter);
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				if (fc.showSaveDialog(jp) == JFileChooser.APPROVE_OPTION) {
					saveGraphAsEPS(fc.getSelectedFile());
				}
			}
		});
		jp.add(salvar);
		return jp;
	}

	public static final void desenha(Tree tree, Transformer nev, String title,
			boolean rotated) {
		SimpleTreeDraw draw = new SimpleTreeDraw(tree, nev, rotated);
		draw.setTitle(title);
		Thread desenho = new Thread(draw, "DESENHO");
		desenho.start();
		synchronized (tree) {
			try {
				tree.wait();
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public void saveGraphAsEPS(File file) {
		// BufferedImage myImage = new BufferedImage(vv.getWidth(),
		// vv.getHeight(), BufferedImage.TYPE_INT_RGB);
		// Graphics2D g2 = myImage.createGraphics();
		// Color bg = vv.getBackground();
		// g2.setColor(bg);
		// g2.fillRect(0, 0, vv.getWidth(), vv.getHeight());
		vvPaths.setDoubleBuffered(true);
		// vv.paintAll(g2);
		try {
			// ImageIO.write(myImage, "gif", new File(filename));
			FileOutputStream fos = new FileOutputStream(file);
			EpsGraphics epsGraphics = new EpsGraphics("TESTE", fos, 0, 0,
					vvPaths.getWidth(), vvPaths.getHeight(),
					ColorMode.COLOR_RGB);
			vvPaths.paintAll(epsGraphics);
			fos.flush();
			epsGraphics.close();
		} catch (Throwable e) {
			System.out.println(e);
		}
	}
}
