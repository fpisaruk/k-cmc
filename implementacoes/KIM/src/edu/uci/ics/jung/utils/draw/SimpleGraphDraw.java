/**
 * 
 */
package edu.uci.ics.jung.utils.draw;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.sf.epsgraphics.ColorMode;
import net.sf.epsgraphics.EpsGraphics;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.algorithms.shortestpath.KIMEdge;
import edu.uci.ics.jung.algorithms.shortestpath.KIMVertex;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.DefaultVisualizationModel;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.BasicVertexLabelRenderer;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class SimpleGraphDraw extends JFrame implements Runnable {
	private static final long serialVersionUID = 5374335358610838068L;
	Graph<KIMVertex, KIMEdge> g;
	private VisualizationViewer<KIMVertex, KIMEdge> vv;
	private MyTransform nev;

	private class MyTransform implements Transformer<KIMEdge, String> {

		private Transformer<KIMEdge, Number> wrapped;

		@Override
		public String transform(KIMEdge arg0) {
			if (wrapped == null)
				return null;
			Object resp = wrapped.transform(arg0);
			if (resp != null)
				return resp.toString();
			else
				return null;
		}

		public MyTransform(Transformer<KIMEdge, Number> transf) {
			wrapped = transf;
		}
	}

	private Layout<KIMVertex, KIMEdge> layout;
	private DefaultModalGraphMouse gm;

	public SimpleGraphDraw(Graph graph, Transformer<KIMEdge, Number> nev) {
		this(graph, nev, new FRLayout<KIMVertex, KIMEdge>(graph, new Dimension(
				600, 600)));
	}

	public SimpleGraphDraw(Graph graph, Transformer<KIMEdge, Number> nev,
			Layout layout) {
		g = graph;
		this.nev = new MyTransform(nev);
		this.layout = layout;
		// pr = new PluggableRenderer();
		vv = new VisualizationViewer(new DefaultVisualizationModel(layout));
		init();

	}

	@Override
	public void run() {
		addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent e) {
				System.out.println("FECHADA");

			}

			@Override
			public void windowClosing(WindowEvent e) {
				System.out.println("FECHANDO");
				synchronized (g) {
					g.notify();
				}
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				System.out.println("DESATIVADA");

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
			}
		});
		pack();
		setVisible(true);
		System.out.println("CRIADA");
	}

	private void init() {
		vv.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex v) {
						return v.getNome();
					}
				});
		vv.getRenderContext().setEdgeLabelTransformer(nev);
		vv.setVertexToolTipTransformer(new ToStringLabeller<KIMVertex>());
		vv.getRenderContext().setVertexFillPaintTransformer(
				new Transformer<KIMVertex, Paint>() {
					@Override
					public Paint transform(KIMVertex v) {
						if (v.isOnPath())
							return Color.RED;
						else
							return Color.BLUE;
					}
				});
		vv.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());

		setBackground(Color.WHITE);
		Font font = new Font(Font.SANS_SERIF, Font.BOLD, 15);
		vv.setBackground(Color.WHITE);
		vv.getRenderContext().setVertexLabelTransformer(
				new Transformer<KIMVertex, String>() {
					@Override
					public String transform(KIMVertex arg0) {
						return arg0.getNome();
					}
				});
		vv.getRenderContext().setEdgeDrawPaintTransformer(
				new MyEdgePaintFunction());
		vv.getRenderContext().setEdgeLabelTransformer(nev);
		vv.setVertexToolTipTransformer(new ToStringLabeller());
		vv.getRenderer().setVertexLabelRenderer(
				new BasicVertexLabelRenderer(Position.AUTO));
		vv.getRenderContext().setLabelOffset(30);
		gm = new DefaultModalGraphMouse();
		gm.setMode(ModalGraphMouse.Mode.PICKING);
		vv.setGraphMouse(gm);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		add(vv, BorderLayout.CENTER);
		add(setUpControls(), BorderLayout.SOUTH);
		pack();
	}

	public class MyEdgePaintFunction implements Transformer<KIMEdge, Paint> {
		public Paint transform(KIMEdge e) {
			if (vv.getGraphLayout().getGraph().getEndpoints(e).getFirst()
					.isOnPath()
					&& vv.getGraphLayout().getGraph().getEndpoints(e)
							.getSecond().isOnPath())
				return Color.RED;
			return Color.BLACK;
		}
	}

	public class EdgeCostLabeller implements Transformer<KIMEdge, String> {
		@Override
		public String transform(KIMEdge arg0) {
			return nev.transform(arg0).toString();
		}
	}

	private JPanel setUpControls() {
		final JPanel jp = new JPanel();
		jp.setBackground(Color.WHITE);
		jp.setLayout(new BoxLayout(jp, BoxLayout.PAGE_AXIS));
		JButton salvar = new JButton("Salvar como eps");
		salvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
						"Encapsulated PostScript", "eps");
				fc.setFileFilter(filter);
				fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
				if (fc.showSaveDialog(jp) == JFileChooser.APPROVE_OPTION) {
					saveGraphAsEPS(fc.getSelectedFile());
				}
			}
		});
		jp.add(salvar);
		return jp;
	}

	public static final void desenha(Graph g, Transformer nev, String title) {
		SimpleGraphDraw draw = new SimpleGraphDraw(g, nev);
		draw.setTitle(title);
		Thread desenho = new Thread(draw);
		desenho.start();
		synchronized (g) {
			try {
				g.wait();
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public static final void desenha(Graph g, Transformer<KIMEdge, Number> nev,
			Layout layout) {
		Thread desenho = new Thread(new SimpleGraphDraw(g, nev, layout),
				"DESENHO");
		desenho.start();
		synchronized (g) {
			try {
				g.wait();
			} catch (InterruptedException e1) {
			}
		}
		desenho.stop();
	}

	public void saveGraphAsEPS(File file) {
		// BufferedImage myImage = new BufferedImage(vv.getWidth(),
		// vv.getHeight(), BufferedImage.TYPE_INT_RGB);
		// Graphics2D g2 = myImage.createGraphics();
		// Color bg = vv.getBackground();
		// g2.setColor(bg);
		// g2.fillRect(0, 0, vv.getWidth(), vv.getHeight());
		vv.setDoubleBuffered(true);
		// vv.paintAll(g2);
		try {
			// ImageIO.write(myImage, "gif", new File(filename));
			FileOutputStream fos = new FileOutputStream(file);
			EpsGraphics epsGraphics = new EpsGraphics("TESTE", fos, 0, 0, vv
					.getWidth(), vv.getHeight(), ColorMode.COLOR_RGB);
			vv.paintAll(epsGraphics);
			fos.flush();
			epsGraphics.close();
		} catch (Throwable e) {
			System.out.println(e);
		}
	}
}
